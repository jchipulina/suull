
var productos = 0;
var productos_lista = new Array();
var producto_selected = 0;
$(document).ready(function () {

	$.fn.inputFilter = function (inputFilter) {
		return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function () {
			if (inputFilter(this.value)) {
				this.oldValue = this.value;
				this.oldSelectionStart = this.selectionStart;
				this.oldSelectionEnd = this.selectionEnd;
			} else if (this.hasOwnProperty("oldValue")) {
				this.value = this.oldValue;
				this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
			} else {
				this.value = "";
			}
		});
	};
	$(".number").inputFilter(function (value) {
		return /^\d*$/.test(value);    // Allow digits only, using a RegExp
	});
	
	/*var elements = $("#listado_detalle tr");
	
	for(var i=0;i<elements.length;i++){
		console.log(elements[i]);
		console.log(elements[i].get(0));
	}*/


	$('#listado_detalle tr').each(function() {
		var arr = $(this).children('td').get(2).getElementsByTagName("input");
		var item = {};
		for(var i=0;i<arr.length;i++){
			
			if(i==0){
				item.id = arr[i].value;
			}
			if(i==1){
				item.sku = arr[i].value;
			}
			if(i==2){
				item.nombre = arr[i].value;
			}
			if(i==3){
				item.cantidad = Number(arr[i].value);
			}
		}
		productos_lista.push(item);
	});
	productos_lista.pop();
	console.log(productos_lista);

	

	$("#cbo_producto").change(onChangeCboProducto);
	$("#btn_agregar_producto").click(onAgregarProducto);
	renderDetalle();
});

function getProductoById(id) {
	for (var i = 0; i < productos.length; i++) {
		if (productos[i].id == id) {
			return productos[i];
		}
	}
}
function onChangeCboProducto() {
	var pid = $("#cbo_producto").val();
	if (pid != "") {
		producto_selected = pid;
	} else {
		producto_selected = 0;
	}
}
function deleteItem(btn) {
	var id = btn.value;
	var nproductos = new Array();
	for (var i = 0; i < productos_lista.length; i++) {
		if (i != id) {
			nproductos.push(productos_lista[i]);
		}
	}
	productos_lista = nproductos;
	renderList();
}
function updateValue(i, cant) {
	productos_lista[i].cantidad = Number(cant);
	if (Number(cant) == 0) {
		deleteItem(i);
	} else {
		renderList();
	}

}
function renderList() {
	var total = 0;
	var listado_detalle_total = "";
	for (var i = 0; i < productos_lista.length; i++) {
		var listado_detalle = '<tr>';
		listado_detalle += '<td>' + productos_lista[i].sku + '</td>';
		listado_detalle += '<td>' + productos_lista[i].nombre + '</td>';
		total += Number(productos_lista[i].cantidad);
		var f = 'updateValue(' + i + ',this.value)';
		var name = "activos"+i;
		var name_ar = "activos["+i+"]";
		listado_detalle += '<td><input type="hidden" value="' + productos_lista[i].id + '" id="'+name+'.id" name="'+name_ar+'.id">';
        listado_detalle += '<input type="hidden" value="' + productos_lista[i].sku + '" id="'+name+'.sku" name="'+name_ar+'.sku">';
        listado_detalle += '<input type="hidden" value="' + productos_lista[i].nombre + '" id="'+name+'.nombre" name="'+name_ar+'.nombre">';
        var s = 'type="text" class="form-control number cantidad" placeholder="Cantidad"';
        listado_detalle += '<input '+s+' id="'+name+'.cantidad" name="'+name_ar+'.cantidad" value="' + productos_lista[i].cantidad + '"></td>';
                                                     
		listado_detalle += '<td><button type="button" value="' + i + '" class="btn btn-default btn-xs" onclick="deleteItem(this)">Eliminar</button></td>';
		listado_detalle += '</tr>';
		listado_detalle_total += listado_detalle;
	}
	var listado_detalle = '<tr>';
	listado_detalle += '<td>Total</td>';
	listado_detalle += '<td></td>';
	listado_detalle += '<td><input type="text" class="form-control cantidad" disabled="" value="' + total + '" /></td>';
	listado_detalle += '<td></td>';
	listado_detalle += '</tr>';
	listado_detalle_total += listado_detalle;
	$("#listado_detalle").html(listado_detalle_total);
	$(".number").inputFilter(function (value) {
		return /^\d*$/.test(value);    // Allow digits only, using a RegExp
	});
}
function addItem(producto, cantidad) {
	var nproducto = { id: producto.id, sku: producto.sku, nombre: producto.nombre, cantidad: cantidad };
	productos_lista.push(nproducto);
	renderList();
}
function onAgregarProducto() {
	if (producto_selected != "") {
		var producto_cantidad = $("#producto_cantidad").val();
		if (producto_cantidad != "") {
			if (!isNaN(producto_cantidad)) {
				var producto = getProductoById(producto_selected);
				$("#producto_cantidad").val('');
				addItem(producto, Number(producto_cantidad));
			} else {
				alert("Cantidad no valida");
			}
		} else {
			alert("Debe de especificar una cantidad")
		}
	} else {
		alert("Debe de seleccionar un producto")
	}
}
function renderDetalle() {
	console.log("onAgregarProducto");
	$.ajax({
		url: '/api/activos',
		type: 'GET',
		dataType: 'json',
		success: function (json) {
			productos = json;
		},
		error: function (xhr, status) {
			console.log(status)
		},
		complete: function (xhr, status) {
			console.log('Petición realizada');
		}
	});

}