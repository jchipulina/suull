package com.tottus.suull.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tottus.suull.entity.ActivoDetalle;
import com.tottus.suull.entity.Devolucion;
import com.tottus.suull.entity.Operacion;
import com.tottus.suull.entity.Tienda;
import com.tottus.suull.entity.Transporte;

public class PDFView extends AbstractPdfView {
	
	private float cellPadding = 1.85f;

	String nro_operacion;
	Tienda local_partida;
	Tienda local_entrega;
	Date fecha_registro;
	Date fecha_inicio_traslado;
	Transporte transporte;
	ArrayList<ActivoDetalle> activos;
	
	@Override
    protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer, HttpServletRequest request, HttpServletResponse response) throws Exception {

		if(model.get("operacion")!=null) {
			Operacion operacion = (Operacion) model.get("operacion");
			local_partida = operacion.getLocal_partida();
			local_entrega = operacion.getLocal_entrega();
			fecha_registro = operacion.getFecha_registro();
			fecha_inicio_traslado = operacion.getFecha_inicio_traslado();
			transporte  = operacion.getTransporte();
			activos = operacion.getActivos();
			nro_operacion = operacion.getNro_operacion();
		}
		if(model.get("devolucion")!=null) {
			Devolucion operacion = (Devolucion) model.get("devolucion");
			local_partida = operacion.getLocal_partida();
			local_entrega = operacion.getLocal_entrega();
			fecha_registro = operacion.getFecha_registro();
			fecha_inicio_traslado = operacion.getFecha_inicio_traslado();
			transporte  = operacion.getTransporte();
			activos = operacion.getActivos();
			nro_operacion = operacion.getNro_operacion();
		}
       
        
        response.setHeader("Content-Disposition", "attachment; filename=\"guia-"+nro_operacion+".pdf\"");
        
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(BaseColor.WHITE);
        
        
        PdfPTable main_table = createTable(3,new float[] {50f,0f,50f});
        main_table.setWidthPercentage(100.0f);
        main_table.setSpacingBefore(0);
        
        main_table.addCell(createTable1());
        main_table.addCell(createCell("",5));
        main_table.addCell(createTable2());
        
       
        PdfPTable second_table = createTable(3,new float[] {5f,75f,20f});
        second_table.setWidthPercentage(100.0f);
        
        second_table.setSpacingBefore(0);
        
        Integer _cellpadding = 5;
        
        for(int i=0;i<activos.size();i++) {
        	 ActivoDetalle activo = activos.get(i);
        	 PdfPTable item = createTable(4,new float[] {15.5f,60.6f,10.9f,13.3f});
             item.addCell(createCellActivo(activo.getSku(),_cellpadding,0));
             item.addCell(createCellActivo(activo.getNombre().toUpperCase(),_cellpadding,0));
             item.addCell(createCellActivo("1",_cellpadding,1));
             item.addCell(createCellActivo(activo.getCantidad()+"",_cellpadding,1));
             
             second_table.addCell(createCellActivo("",5,0));
             second_table.addCell(item);
             second_table.addCell(createCellActivo("",_cellpadding,0));
        }
       

        document.add(main_table);
        Chunk chuck = new Chunk();
        Font _font = FontFactory.getFont(FontFactory.HELVETICA);
		_font.setSize(6);
        Phrase phrase = new Phrase(new Chunk(" ",_font));
        Paragraph title = new Paragraph(phrase);
        title.setPaddingTop(100);
        document.add(title);
        document.add(title);
        document.add(title);
        document.add(title);
        document.add(title);
        document.add(second_table);
        
        
    }

	
	
	PdfPTable createTable1() {

		PdfPTable table = createTable(1);
		PdfPTable t = createTable(2,new float[] {25f, 80f});
		t.addCell(createCellColor(" ",cellPadding,BaseColor.WHITE));
		t.addCell(createCell(this.local_partida.getDireccion().toUpperCase(),cellPadding));
		table.addCell(t);
		
		t = createTable(2,new float[] {20f, 80f});
		t.addCell(createCell(" ",cellPadding));//OTROS
		t.addCell(createCell(" ",cellPadding));
		table.addCell(t);
		t = createTable(2,new float[] {20f, 80f});
		t.addCell(createCell(" ",cellPadding));//OTROS
		t.addCell(createCell(" ",cellPadding));
		table.addCell(t);

		t = createTable(2,new float[] {20f, 80f});
		t.addCell(createCell("",cellPadding));//NOMBRE
		t.addCell(createCell(this.local_entrega.getNombre().toUpperCase(),cellPadding));
		table.addCell(t);
		
		t = createTable(2,new float[] {20f, 80f});
		t.addCell(createCell("",cellPadding));//DIRECCION
		t.addCell(createCell(this.local_entrega.getDireccion().toUpperCase(),cellPadding));
		table.addCell(t);
		
		t = createTable(4,new float[] {20f,45f,10f,25f});
		t.addCell(createCell("",cellPadding));//DISTRITO
		t.addCell(createCell("MIRAFLORES",cellPadding));
		t.addCell(createCell("",cellPadding));//PRO
		t.addCell(createCell("LIMA",cellPadding));
		table.addCell(t);
		
		t = createTable(4,new float[] {20f,45f,10f,25f});
		t.addCell(createCell("",cellPadding));//RUC
		t.addCell(createCell(this.local_entrega.getRuc(),cellPadding));
		t.addCell(createCell("",cellPadding));//DEP
		t.addCell(createCell("LIMA",cellPadding));
		table.addCell(t);
		
		t = createTable(2,new float[] {20f, 80f});
		t.addCell(createCell("",cellPadding));//OTROS
		t.addCell(createCell("",cellPadding));
		table.addCell(t);	

        return table;
	}
	PdfPTable createTable2() {
        
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		PdfPTable table = createTable(1);
		
		PdfPTable t = createTable(4,new float[] {20f,40f,20f,20f});
		t.addCell(createCellColor("  ",cellPadding,BaseColor.WHITE));
		t.addCell(createCell(dateFormat.format(this.fecha_registro),cellPadding));
		t.addCell(createCellColor("  ",cellPadding,BaseColor.WHITE));
		t.addCell(createCell(dateFormat.format(this.fecha_inicio_traslado),cellPadding));
		table.addCell(t);
		
		t = createTable(2,new float[] {20f, 80f});
		t.addCell(createCell(" ",cellPadding));//OTROS
		t.addCell(createCell(" ",cellPadding));
		table.addCell(t);	
		t = createTable(2,new float[] {20f, 80f});
		t.addCell(createCell(" ",cellPadding));//OTROS
		t.addCell(createCell(" ",cellPadding));
		table.addCell(t);

		
		
		t = createTable(2,new float[] {20f, 80f});
		t.addCell(createCell("",cellPadding));//NOMBRE
		t.addCell(createCell(this.transporte.getRazon_social().toUpperCase(),cellPadding));
		table.addCell(t);
		
		t = createTable(4,new float[] {20f,40f,10f,30f});
		t.addCell(createCell("",cellPadding));//RUC
		t.addCell(createCell(this.transporte.getRuc(),cellPadding));
		t.addCell(createCell("",cellPadding));//MARCA
		t.addCell(createCell(this.transporte.getMarca().toUpperCase(),cellPadding));
		table.addCell(t);
		
		t = createTable(2,new float[] {20f, 80f});
		t.addCell(createCell("",cellPadding));//CHOFER
		t.addCell(createCell(this.transporte.getChofer().toUpperCase(),cellPadding));
		table.addCell(t);
		
		t = createTable(2,new float[] {40f, 60f});
		t.addCell(createCell("",cellPadding));//LICENCIA DE CONDUCIR
		t.addCell(createCell(this.transporte.getLicencia().toUpperCase(),cellPadding));
		table.addCell(t);
		
		t = createTable(4,new float[] {20f,45f,10f,25f});
		t.addCell(createCell("",cellPadding));//PLACA
		t.addCell(createCell(this.transporte.getPlaca().toUpperCase(),cellPadding));
		t.addCell(createCell("",cellPadding));//FIRMA
		t.addCell(createCell("",cellPadding));
		table.addCell(t);

        return table;
	}
	PdfPCell createCell(String contenido,float padding,float paddingLeft) { 
		return _createCell(0,contenido,BaseColor.BLACK,padding,paddingLeft,padding);
	}
	PdfPCell createCellActivo(String contenido,float padding,Integer align) { 
		return _createCell(align,contenido,BaseColor.BLACK,padding,padding,16);
	}
	PdfPCell createCell(String contenido,float padding) { 
		return _createCell(0,contenido,BaseColor.BLACK,padding,padding,padding);
	}
	PdfPCell createCellColor(String contenido,float padding,BaseColor color) { 
		return _createCell(0,contenido,BaseColor.WHITE,padding,padding,padding);
	}
	PdfPCell createCellColor(String contenido,float padding,BaseColor color,float paddingLeft) { 
		return _createCell(0,contenido,BaseColor.WHITE,padding,paddingLeft,padding);
	}
	PdfPCell _createCell(Integer align,String contenido,BaseColor color,float _padding,float _padding_left,float _padding_bottom) { 
		Font font = FontFactory.getFont(FontFactory.HELVETICA);
		font.setColor(color);
		font.setSize(8);
		PdfPCell cell = new PdfPCell();
		
		cell.setHorizontalAlignment(align);
		cell.setBorderWidth(0);
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setBackgroundColor(BaseColor.WHITE);
        cell.setPhrase(new Phrase(contenido, font));
        
        
        cell.setPaddingLeft(_padding_left);
        cell.setPaddingRight(_padding);
        cell.setPaddingTop(_padding);
        cell.setPaddingBottom(_padding_bottom);
        
        //cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
        //cell.setPadding(padding);
        
		return cell;
	}
	
	
	PdfPTable createTable(Integer rows,float[] anchoDeColumnas) {
		PdfPTable table = new PdfPTable(rows);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorderColor(BaseColor.WHITE);
		
		//table.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);
		table.getDefaultCell().setPadding(0);
		
		if(anchoDeColumnas.length>0) {
			 try {
				 table.setWidths(anchoDeColumnas);
				 
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return table;
	}
	PdfPTable createTable(Integer rows) {
		PdfPTable table = new PdfPTable(rows);
		table.getDefaultCell().setBorder(0);
		table.getDefaultCell().setBorderColor(BaseColor.WHITE);
		
		//table.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);
		table.getDefaultCell().setPadding(0);
		return table;
	}
}
