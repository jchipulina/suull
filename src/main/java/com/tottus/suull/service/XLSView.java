package com.tottus.suull.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import com.tottus.suull.controllers.AlquileresController;
import com.tottus.suull.entity.Activo;
import com.tottus.suull.entity.ReporteProveedor;
import com.tottus.suull.entity.Tienda;
import com.tottus.suull.entity.ReporteMovimientos;

public class XLSView extends AbstractXlsView{

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {

    	Integer tipo_reporte =  (Integer) model.get("reporte");
    	
    	if(tipo_reporte==1) {
            response.setHeader("Content-Disposition", "attachment; filename=\"rebate.xls\"");

            @SuppressWarnings("unchecked")
            List<ReporteProveedor> data = (List<ReporteProveedor>) model.get("data");
            
            Sheet sheet = workbook.createSheet("Rebate");
            sheet.setDefaultColumnWidth(30);

            CellStyle style = workbook.createCellStyle();
            Font font = workbook.createFont();
            font.setFontName("Arial");
            style.setFillForegroundColor(HSSFColor.BLUE.index);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            font.setBold(true);
            font.setColor(HSSFColor.WHITE.index);
            style.setFont(font);

            Row header = sheet.createRow(0);

            String[] header_data = {	
            		"ID REBATE",
            		"DESCRIPCIÓN REBATE",
            		"CODIGO PROVEEDOR",
            		"ID CONTRATO",
            		"DESCRIPCION",
            		"CONTRATO",
            		"COMPRADOR",
            		"METODO",
            		"RECLAMO",
            		"NUMERO LOCAL",
            		"TIPO REBATE",
            		"FECHA INICIO REBATE",
            		"FECHA FIN REBATE",
            		"CODIGO PRODUCTO",
            		"MONTO REBATE",
            		"Base de cálculo ",
            		"Método de asignación ",
            		"Cod. Marca",
            		"Porcentaje de asignación ",
            		"Nro de Rebate",
            		"Ind. De provisión",
            		"Año",
            		"Periodo",
            		"Dato de control",
            };
            
            for(int i=0;i<header_data.length;i++) {
            	header.createCell(i).setCellValue(header_data[i]);
                header.getCell(i).setCellStyle(style);
            }

            Integer rowCount = 1;

            if(data!=null) {
            	for(ReporteProveedor dto : data){
            		
            		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            		
            		String strInicio= dateFormat.format(dto.getFecha_inicio()); 
            		String strFin= dateFormat.format(dto.getFecha_fin()); 
            		
            		
            		for(int i=0;i<dto.getMontos().size();i++) {
            			rowCount = createRow(strInicio,strFin,dto.getMontos().get(i).activo.getSku(),dto.getMontos().get(i).monto,dto,sheet,rowCount);
            		}
                    
                }
            }
    	}
    	if(tipo_reporte==2) {
    		response.setHeader("Content-Disposition", "attachment; filename=\"stock_general.xls\"");

            @SuppressWarnings("unchecked")
            List<Tienda> tiendas = (List<Tienda>) model.get("data");
            @SuppressWarnings("unchecked")
			List<Activo> activos = (List<Activo>) model.get("activos");
            
            Sheet sheet = workbook.createSheet("Rebate");
            sheet.setDefaultColumnWidth(30);

            CellStyle style = workbook.createCellStyle();
            Font font = workbook.createFont();
            font.setFontName("Arial");
            style.setFillForegroundColor(HSSFColor.BLUE.index);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            font.setBold(true);
            font.setColor(HSSFColor.WHITE.index);
            style.setFont(font);

            Row header = sheet.createRow(0);

            ArrayList<String> header_data = new ArrayList<String>();
            
            header_data.add("Tienda");
            for(int i=0;i<activos.size();i++) {
            	header_data.add(activos.get(i).getSku());
            }
            header_data.add("Stock Total");

            
            for(int i=0;i<header_data.size();i++) {
            	header.createCell(i).setCellValue(header_data.get(i));
                header.getCell(i).setCellStyle(style);
            }

            for(int p=0;p<tiendas.size();p++) {
            	Tienda tienda = tiendas.get(p);
            	 Row _row =  sheet.createRow(p+1);
                 _row.createCell(0).setCellValue(tienda.getNombre());
                 for(int i=0;i<activos.size();i++) {
                	 Activo ac = activos.get(i);
                	 String value = "0";
                	 for(int u=0;u<tienda.getStocks().size();u++) {
                		 String sku = tienda.getStocks().get(u).getSku();
                		 if(sku.equals(ac.getSku())) {
                			 value = ""+tienda.getStocks().get(u).getStock();
                			 break;
                		 }
                	 }
                  	_row.createCell(i+1).setCellValue(value);
                  }
                 _row.createCell(activos.size()+1).setCellValue(""+tienda.getCantidad_activos());
            }
            

    	}
    	if(tipo_reporte==3) {
    		response.setHeader("Content-Disposition", "attachment; filename=\"movimientos.xls\"");

            @SuppressWarnings("unchecked")
            List<ReporteMovimientos> data = (List<ReporteMovimientos>) model.get("data");
            
            Sheet sheet = workbook.createSheet("Rebate");
            sheet.setDefaultColumnWidth(30);

            CellStyle style = workbook.createCellStyle();
            Font font = workbook.createFont();
            font.setFontName("Arial");
            style.setFillForegroundColor(HSSFColor.BLACK.index);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            font.setBold(true);
            font.setColor(HSSFColor.WHITE.index);
            style.setFont(font);

            Row header = sheet.createRow(0);
    		
            //"nro_operacion_origen",
            //"nro_precinto",
    		String[] header_data = {	
    				"nro_operacion",
    	    		"tipo_movimiento",
    	    		"tipo_movimiento_final",
    	    		"origen_loc",
    	    		"destino_loc",
    	    		"sku",
    	    		"stock_origen",
    	    		"stock_ingreso",
    	    		"stock_salida",
    	    		"stock_discrepancia",
    	    		
    	    		"fecha_emision",
    	    		"fecha_recepcion",
    	    		"observacion",
    	    		"ruc_emptransp",
    	    		"razon_social",
    	    		"nro_licencia",
    	    		"nom_chofer",
    	    		"marca",
    	    		"nro_placa",
    	    		"nro_guia_remision",
    	    		
    	    		"usuario"
            };
    		for(int i=0;i<header_data.length;i++) {
            	header.createCell(i).setCellValue(header_data[i]);
                header.getCell(i).setCellStyle(style);
            }
    		Integer rowCount = 0;
    		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    		for(int p=0;p<data.size();p++) {
    			ReporteMovimientos rp = data.get(p);
    			
    			Date f1 = rp.getFecha_emision();
    			Date f2 = rp.getFecha_recepcion();
    			
    			String strFechaEmision = ""; 
        		String strFechaRecepcion = ""; 
        		
        		
    			if(f1!=null) {
    				strFechaEmision = dateFormat.format(rp.getFecha_emision()); 
    			}
    			if(f2!=null) {
    				strFechaRecepcion = dateFormat.format(rp.getFecha_recepcion()); 
    			}
    			
    			
        		
            	 Row _row =  sheet.createRow(p+1);
                 _row.createCell(rowCount++).setCellValue(rp.getNro_operacion());
                 _row.createCell(rowCount++).setCellValue(rp.getTipo_movimiento());
                 _row.createCell(rowCount++).setCellValue(rp.getTipo_movimiento_final());
                 _row.createCell(rowCount++).setCellValue(rp.getOrigen_loc());
                 _row.createCell(rowCount++).setCellValue(rp.getDestino_loc());
                 _row.createCell(rowCount++).setCellValue(rp.getSku());
                 _row.createCell(rowCount++).setCellValue(rp.getOh_origen());
                 _row.createCell(rowCount++).setCellValue(rp.getOh_ingreso());
                 _row.createCell(rowCount++).setCellValue(rp.getOh_salida());
                 _row.createCell(rowCount++).setCellValue(rp.getOh_discrepancia());
                 
                 //_row.createCell(rowCount++).setCellValue(rp.getNro_operacion_origen());
                 _row.createCell(rowCount++).setCellValue(strFechaEmision);
                 _row.createCell(rowCount++).setCellValue(strFechaRecepcion);
                 _row.createCell(rowCount++).setCellValue(rp.getObservacion());
                 _row.createCell(rowCount++).setCellValue(rp.getRuc_emptransp());
                 _row.createCell(rowCount++).setCellValue(rp.getRazon_social());
                 _row.createCell(rowCount++).setCellValue(rp.getNro_licencia());
                 _row.createCell(rowCount++).setCellValue(rp.getNom_chofer());
                 _row.createCell(rowCount++).setCellValue(rp.getMarca());
                 _row.createCell(rowCount++).setCellValue(rp.getNro_placa());
                 
                 _row.createCell(rowCount++).setCellValue(rp.getNro_guia_remision());
                // _row.createCell(rowCount++).setCellValue(rp.getNro_precinto());
                 _row.createCell(rowCount++).setCellValue(rp.getUsuario().getEmail());
                 
                 rowCount=0;
            }

    		
    		
    	}

        

    }
    private Integer createRow(String fini,String fend,String SKU,Double monto,ReporteProveedor dto,Sheet sheet,Integer rowCount) {
    	
        /*"ID_REBATE",
		"DESCRIPCIÓN REBATE",
		"CODIGO PROVEEDOR",
		"ID CONTRATO",
		"DESCRIPCION",
		"CONTRATO",
		"COMPRADOR",
		"METODO",
		"RECLAMO",
		"NUMERO LOCAL",
		"TIPO REBATE",
		"FECHA INICIO REBATE",
		"FECHA FIN REBATE",
		"CODIGO PRODUCTO",
		"MONTO REBATE",
		"Base de cálculo ",
		"Método de asignación ",
		"Cod. Marca",
		"Porcentaje de asignación ",
		"Nro de Rebate",
		"Ind. De provisión",
		"Año",
		"Periodo",
		"Dato de control",*/
    	Row _row =  sheet.createRow(rowCount);
        _row.createCell(0).setCellValue(rowCount);
        _row.createCell(1).setCellValue("C.D.FRESCOSALQUILERDEJABAS");
        _row.createCell(2).setCellValue(dto.getRuc_proveedor());
        _row.createCell(3).setCellValue("");//ID CONTRATO
        _row.createCell(4).setCellValue("ALQUILER MAT LOG CD FRESCOS");//DESCRIPCION
        _row.createCell(5).setCellValue("");//CONTRATO
        _row.createCell(6).setCellValue(dto.getUsuario());//COMPRADOR
        _row.createCell(7).setCellValue(rowCount);//METODO
        _row.createCell(8).setCellValue(rowCount);//RECLAMO
        _row.createCell(9).setCellValue("569");//NUMERO LOCAL
        _row.createCell(10).setCellValue("AP LOG MAT");//TIPO REBATE
        _row.createCell(11).setCellValue(fini);
        _row.createCell(12).setCellValue(fend);
        _row.createCell(13).setCellValue(SKU);
        _row.createCell(14).setCellValue(monto);
        _row.createCell(15).setCellValue(1);
        _row.createCell(16).setCellValue(20);
        _row.createCell(17).setCellValue(1);
        _row.createCell(19).setCellValue("");
        _row.createCell(20).setCellValue("");
        _row.createCell(21).setCellValue("");
        _row.createCell(22).setCellValue("");
        _row.createCell(23).setCellValue("");
        _row.createCell(24).setCellValue("");
        
        
      //1	C.D.FRESCOSALQUILERDEJABAS	2012216084		ALQUILER MAT LOG CD FRESCOS	jvalenza		569	AP LOG MAT	1/10/2020	1/10/2020	J04	42	1	20	1
        rowCount++;
        return rowCount;
    }
}