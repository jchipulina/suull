package com.tottus.suull.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.Cliente;

public interface RepoCliente extends MongoRepository<Cliente, String> {

}
