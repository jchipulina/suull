package com.tottus.suull.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.Activo;
import com.tottus.suull.entity.Alquiler;

public interface RepoActivo extends MongoRepository<Activo, String> {
	Activo findBySku(String sku);
	public List<Activo> findByAlquiler(boolean flag);
}
