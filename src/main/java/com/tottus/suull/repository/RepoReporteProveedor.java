package com.tottus.suull.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.ReporteProveedor;

public interface RepoReporteProveedor extends MongoRepository<ReporteProveedor, String> {

}
