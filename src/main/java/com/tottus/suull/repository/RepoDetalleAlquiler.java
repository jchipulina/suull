package com.tottus.suull.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.DetalleAlquiler;

public interface RepoDetalleAlquiler extends MongoRepository<DetalleAlquiler, String> {

}
