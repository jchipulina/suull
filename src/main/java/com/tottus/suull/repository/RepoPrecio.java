package com.tottus.suull.repository;


import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.Precio;

public interface RepoPrecio extends MongoRepository<Precio, String> {
	
}
