package com.tottus.suull.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.Users;

public interface RepoUsers extends MongoRepository<Users, String> {
  Users findByUsername(String username);
}