package com.tottus.suull.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.tottus.suull.entity.Movimiento;

public interface RepoMovimiento extends MongoRepository<Movimiento, String> {
	
	@Query("{'fecha': {$gte: ?0, $lte:?1 }}")
	List<Movimiento> findFechaBetween(Date startDate, Date endDate);
	
	@Query(value = "{ 'proveedor.id' : ?0, 'activo.id' : ?1 }")
	public List<Movimiento> findByProveedorAndActivo(String proveedor,String activo);
	
	@Query(value = "{ 'proveedor.id' : ?0 }",sort = "{'fecha': -1}")
	public List<Movimiento> findByProveedorOrderByFechaDesc(String proveedor);
	
	@Query(value = "{ 'alquiler.id' : ?0 }")
	public List<Movimiento> findByAlquiler(String alquiler);
	
	@Query(value = "{ 'proveedor.id' : ?0, 'activo.id' : ?1 , 'fecha': {$gte: ?2, $lte:?3 }}")
	public List<Movimiento> findByProveedorAndActivoAndFechaBetween(String proveedor,String activo,Date startDate, Date endDate);
	
	@Query(value = "{ 'proveedor.id' : ?0, 'activo.id' : ?1 , 'fecha': {$lt:?2 }}")
	public List<Movimiento> findByProveedorAndActivoAndFecha(String proveedor,String activo,Date currentDate);
	
}
