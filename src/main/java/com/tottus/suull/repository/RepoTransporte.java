package com.tottus.suull.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.Transporte;

public interface RepoTransporte extends MongoRepository<Transporte, String> {

}
