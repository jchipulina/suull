package com.tottus.suull.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.Compra;

public interface RepoCompra extends MongoRepository<Compra, String> {

}
