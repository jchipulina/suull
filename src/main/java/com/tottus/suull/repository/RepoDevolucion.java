package com.tottus.suull.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.tottus.suull.entity.Devolucion;

public interface RepoDevolucion extends MongoRepository<Devolucion, String> {
	public List<Devolucion> findByEstado(String estado);
	
	@Query("{'fecha_registro': {$gte: ?0, $lte:?1 }}")
	List<Devolucion> findByFechaRegistroBetween(Date startDate, Date endDate);
	
	@Query(value = "{ 'estado' : ?0, 'local_entrega_id' : ?1 }")
	public List<Devolucion> findByEstadoAndLocalentregaid(String estado,String local_entrega_id);
	
	@Query(value = "{ 'estado' : ?0, 'local_partida_id' : ?1 }")
	public List<Devolucion> findByEstadoAndLocalpartidaid(String estado,String local_partida_id);
}
