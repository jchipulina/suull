package com.tottus.suull.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.Proveedor;

public interface RepoProveedor extends MongoRepository<Proveedor, String> {

}
