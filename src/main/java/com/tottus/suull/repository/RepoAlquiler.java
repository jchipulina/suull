package com.tottus.suull.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.tottus.suull.entity.Activo;
import com.tottus.suull.entity.Alquiler;

public interface RepoAlquiler extends MongoRepository<Alquiler, String> {
	@Query(value = "{ 'estado' : ?0 }")
	public List<Alquiler> findByEstado(String estado);
	@Query(value = "{ 'operacion' : ?0 }")
	public List<Alquiler> findByOperacion(String operacion);
	
	
	@Query(value = "{ 'estado' : ?0, 'operacion' : ?1 }")
	public List<Alquiler> findByEstadoAndOperacion(String estado,String operacion);
	
	//@Query(value = "{ 'proveedor.id' : ?0 }")
	//public Alquiler findFirstByProveedorAndOrderByFecha_registro(String id_proveedor);
	
	//public Alquiler findFirstByAndOrderByFecha_registro();
	
	//public List<Alquiler> findAllOrderByFecha();
}
