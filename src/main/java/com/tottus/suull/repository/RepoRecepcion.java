package com.tottus.suull.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.tottus.suull.entity.Operacion;

public interface RepoRecepcion extends MongoRepository<Operacion, String> {
	@Query(value = "{ 'estado' : ?0, 'local_entrega_id' : ?1 }")
	public List<Operacion> findByEstadoAndLocalentregaid(String estado,String local_entrega_id);
}
