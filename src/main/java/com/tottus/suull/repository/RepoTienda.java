package com.tottus.suull.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.Tienda;

public interface RepoTienda extends MongoRepository<Tienda, String> {
	public Tienda findByNombre(String nombre);
	public Optional<Tienda> findByNumero(Integer numero);
}
