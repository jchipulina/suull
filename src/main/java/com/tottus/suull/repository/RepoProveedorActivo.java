package com.tottus.suull.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.Proveedor;
import com.tottus.suull.entity.ProveedorActivo;

public interface RepoProveedorActivo extends MongoRepository<ProveedorActivo, String> {

}
