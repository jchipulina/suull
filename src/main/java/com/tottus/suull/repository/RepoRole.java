package com.tottus.suull.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.tottus.suull.entity.Role;

/**
 *
 * @author didin
 */
public interface RepoRole extends MongoRepository<Role, String> {
    
    Role findByRole(String role);
}
