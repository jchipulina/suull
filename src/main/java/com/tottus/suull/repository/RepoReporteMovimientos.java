package com.tottus.suull.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.tottus.suull.entity.ReporteMovimientos;

public interface RepoReporteMovimientos extends MongoRepository<ReporteMovimientos, String> {

}
