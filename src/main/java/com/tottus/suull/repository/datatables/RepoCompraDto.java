package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.Compra;

public interface RepoCompraDto extends DataTablesRepository<Compra, String> {

}
