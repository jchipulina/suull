package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.Recepcion;

public interface RepoRecepcionDto extends DataTablesRepository<Recepcion, String> {

}
