package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.ProveedorActivo;

public interface RepoProveedorActivoDto extends DataTablesRepository<ProveedorActivo, String> {

}
