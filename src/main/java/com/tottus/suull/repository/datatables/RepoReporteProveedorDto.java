package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.ReporteProveedor;

public interface RepoReporteProveedorDto extends DataTablesRepository<ReporteProveedor, String> {

}
