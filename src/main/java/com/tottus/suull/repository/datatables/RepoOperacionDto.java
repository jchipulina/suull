package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.Operacion;

public interface RepoOperacionDto extends DataTablesRepository<Operacion, String> {
	
}
