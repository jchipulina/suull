package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.Cliente;

public interface RepoClienteDto extends DataTablesRepository<Cliente, String> {

}
