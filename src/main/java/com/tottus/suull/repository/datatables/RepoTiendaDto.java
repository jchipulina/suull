package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.Tienda;

public interface RepoTiendaDto extends DataTablesRepository<Tienda, String> {

}
