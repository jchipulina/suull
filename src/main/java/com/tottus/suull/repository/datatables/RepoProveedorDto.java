package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.Proveedor;

public interface RepoProveedorDto extends DataTablesRepository<Proveedor, String> {

}
