package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.Alquiler;

public interface RepoAlquilerDto extends DataTablesRepository<Alquiler, String> {

}
