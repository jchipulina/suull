package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.Transporte;

public interface RepoTransporteDto extends DataTablesRepository<Transporte, String> {

}
