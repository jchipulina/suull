package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.Devolucion;


public interface RepoDevolucionDto extends DataTablesRepository<Devolucion, String> {
	
}
