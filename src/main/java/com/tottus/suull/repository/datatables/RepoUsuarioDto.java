package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.Users;

public interface RepoUsuarioDto extends DataTablesRepository<Users, String> {

}
