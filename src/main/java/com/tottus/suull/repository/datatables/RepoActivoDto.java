package com.tottus.suull.repository.datatables;

import org.springframework.data.mongodb.datatables.DataTablesRepository;

import com.tottus.suull.entity.Activo;

public interface RepoActivoDto extends DataTablesRepository<Activo, String> {

}
