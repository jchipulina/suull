package com.tottus.suull.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.validation.Valid;
import java.time.temporal.ChronoUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.datatables.DataTablesInput;
import org.springframework.data.mongodb.datatables.DataTablesOutput;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tottus.suull.entity.Activo;
import com.tottus.suull.entity.ActivoRef;
import com.tottus.suull.entity.Alquiler;
import com.tottus.suull.entity.Cliente;
import com.tottus.suull.entity.Compra;
import com.tottus.suull.entity.Devolucion;
import com.tottus.suull.entity.Movimiento;
import com.tottus.suull.entity.Op;
import com.tottus.suull.entity.Operacion;
import com.tottus.suull.entity.Precio;
import com.tottus.suull.entity.Proveedor;
import com.tottus.suull.entity.ProveedorActivo;
import com.tottus.suull.entity.Rebate;
import com.tottus.suull.entity.Recepcion;
import com.tottus.suull.entity.ReporteMovimientos;
import com.tottus.suull.entity.ReporteProveedor;
import com.tottus.suull.entity.RespCreateMonto;
import com.tottus.suull.entity.Role;
import com.tottus.suull.entity.Tienda;
import com.tottus.suull.entity.Transporte;
import com.tottus.suull.entity.Users;
import com.tottus.suull.repository.RepoActivo;
import com.tottus.suull.repository.RepoAlquiler;
import com.tottus.suull.repository.RepoDevolucion;
import com.tottus.suull.repository.RepoMovimiento;
import com.tottus.suull.repository.RepoOperacion;
import com.tottus.suull.repository.RepoPrecio;
import com.tottus.suull.repository.RepoProveedor;
import com.tottus.suull.repository.RepoProveedorActivo;
import com.tottus.suull.repository.RepoReporteMovimientos;
import com.tottus.suull.repository.RepoReporteProveedor;
import com.tottus.suull.repository.RepoRole;
import com.tottus.suull.repository.RepoUsers;
import com.tottus.suull.repository.datatables.RepoActivoDto;
import com.tottus.suull.repository.datatables.RepoAlquilerDto;
import com.tottus.suull.repository.datatables.RepoClienteDto;
import com.tottus.suull.repository.datatables.RepoCompraDto;
import com.tottus.suull.repository.datatables.RepoDevolucionDto;
import com.tottus.suull.repository.datatables.RepoOperacionDto;
import com.tottus.suull.repository.datatables.RepoProveedorActivoDto;
import com.tottus.suull.repository.datatables.RepoProveedorDto;
import com.tottus.suull.repository.datatables.RepoRecepcionDto;
import com.tottus.suull.repository.datatables.RepoTiendaDto;
import com.tottus.suull.repository.datatables.RepoTransporteDto;
import com.tottus.suull.repository.datatables.RepoUsuarioDto;
import com.tottus.suull.service.SequenceGeneratorService;
import com.tottus.suull.repository.datatables.RepoReporteProveedorDto;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@RestController
@RequestMapping("api")
public class Rest {
	
	@Autowired private RepoUsers repoUsers;
	@Autowired private RepoActivo repoActivo;
	
	@Autowired private RepoMovimiento repoMovimiento;
	@Autowired private RepoAlquiler repoAlquiler;
	@Autowired private RepoReporteProveedor repoReporteProveedor;
	@Autowired private RepoReporteMovimientos repoReporteMovimientos;
	
	@Autowired private RepoOperacionDto operacionDTO;
	@Autowired private RepoDevolucionDto devolucionDTO;
	
	@Autowired private RepoTiendaDto tiendaDTO;
	@Autowired private RepoTransporteDto transporteDTO;
	@Autowired private RepoActivoDto activoDTO;
	@Autowired private RepoUsuarioDto usersDTO;
	@Autowired private RepoRecepcionDto recepcionDTO;
	
	@Autowired private RepoClienteDto repoClienteDTO;
	@Autowired private RepoProveedorDto repoProveedorDTO;
	@Autowired private RepoReporteProveedorDto repoReporteProveedorDTO;
	@Autowired private RepoProveedorActivoDto repoProveedorActivoDTO;
	
	@Autowired private RepoProveedor repoProveedor;
	@Autowired private RepoProveedorActivo repoProveedorActivo;

	@Autowired private RepoPrecio repoPrecio;
	
	@Autowired private RepoCompraDto repoCompraDTO;
	@Autowired private RepoAlquilerDto repoAlquilerDTO;
	
	 @Autowired private RepoOperacion repoOperacion;
	 @Autowired private RepoDevolucion repoDevolucion;
	
	 @Autowired
	    private RepoRole roleRepository;
	   // @Autowired
	   // private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	private SequenceGeneratorService sequenceGenerator;
	//$2a$10$zBJzxZ76YopHAkoEwdm1M.075dRgHaKW/UeSVP9/QlQ9cO68SFDxy
	
	 @Autowired
	    public Rest(SequenceGeneratorService sequenceGeneratorService) {
	        this.sequenceGenerator = sequenceGeneratorService;
	    }
	
	@RequestMapping(value = "/encode/{pass}", method = RequestMethod.GET)
    public CustomResponse encodePass(@PathVariable("pass")String pass) {
		
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encoded = encoder.encode(pass);
		CustomResponse cr = new CustomResponse();
		cr.setResponse(encoded);
		return cr;
    }
	
	@RequestMapping(value = "/ultimo_movimiento/{id}", method = RequestMethod.GET)
    public CustomResponse ultimoMovimiento(@PathVariable("id")String id) {
		String fecha_ultimo_movimiento = "";
		ArrayList<Movimiento> movimientos = (ArrayList<Movimiento>) this.repoMovimiento.findByProveedorOrderByFechaDesc(id);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		boolean flag = false;
		for(int i=0;i<movimientos.size();i++) {
			flag = true;
			fecha_ultimo_movimiento = dateFormat.format((movimientos.get(i).getFecha()));
		}
		CustomResponse cr = new CustomResponse();
		if(flag) {
			cr.setStatus("OK");
			cr.setResponse(fecha_ultimo_movimiento);
		}else {
			cr.setStatus("NO_MOVIMIENTOS");
		}
		return cr;
    }
	
	@RequestMapping(value = "/proveedor/{id}", method = RequestMethod.GET)
    public Proveedor getProveedor(@PathVariable("id")String id) {
		return repoProveedor.findById(id).get();
    }
	@RequestMapping(value = "/proveedor_activo/{id}", method = RequestMethod.GET)
    public ProveedorActivo getProveedorActivo(@PathVariable("id")String id) {
		return repoProveedorActivo.findById(id).get();
    }
	
	
	@RequestMapping(value = "/despachos_dto/{estado}", method = RequestMethod.GET)
    public DataTablesOutput<Operacion> listOperaciones(@Valid DataTablesInput input,@PathVariable("estado")String estado) {
		
		String s_auth  = SecurityContextHolder.getContext().getAuthentication().getAuthorities().iterator().next().getAuthority();
		boolean tienda = false;
		Criteria c1=null;
		System.out.print("s_auth "+s_auth);
		if(s_auth.equals("ROLE_SUULL_TIENDA")) {
			c1 = where("local_entrega_id").is(getCurrentTienda().getId());
		}
		if(s_auth.equals("ROLE_SUULL_TIENDACENTRAL")) {
			c1 = where("local_partida_id").is(getCurrentTienda().getId());
		}
		Criteria c2 = where("estado").is(estado);
		
		if(estado.equals("ABIERTO")) {
			return operacionDTO.findAll(input,c1,c2);
		}else if(estado.equals("CERRADO")) {
			return operacionDTO.findAll(input,c1,c2);
		}else if(estado.equals("OBSERVADO")) {
			return operacionDTO.findAll(input,c1,c2);
		}else if(estado.equals("RECEPCIONADO")) {
			return operacionDTO.findAll(input,c1,c2);
		}else {
			if(s_auth.equals("ROLE_SUULL_TIENDA")) {
				Criteria c3 = where("estado").ne("ABIERTO");
				return operacionDTO.findAll(input,c1,c3);
			}else {
				return operacionDTO.findAll(input,c1);
			}
			
		}
		
    }
	@RequestMapping(value = "/devoluciones_dto/{estado}", method = RequestMethod.GET)
    public DataTablesOutput<Devolucion> listDevoluciones(@Valid DataTablesInput input,@PathVariable("estado")String estado) {
		
		String s_auth  = SecurityContextHolder.getContext().getAuthentication().getAuthorities().iterator().next().getAuthority();
		boolean tienda = false;
		Criteria c1=null;
		System.out.print("s_auth "+s_auth);
		if(s_auth.equals("ROLE_SUULL_TIENDACENTRAL")) {
			Tienda tienda_origen = getCurrentTienda();
			c1 = where("local_entrega_id").is(tienda_origen.getId());
		}
		
		if(s_auth.equals("ROLE_SUULL_TIENDA")) {
			c1 = where("local_partida_id").is(getCurrentTienda().getId());
		}
		
		
		Criteria c2 = where("estado").is(estado);
		
		if(estado.equals("ABIERTO")) {
			return devolucionDTO.findAll(input,c1,c2);
		}else if(estado.equals("CERRADO")) {
			return devolucionDTO.findAll(input,c1,c2);
		}else if(estado.equals("OBSERVADO")) {
			return devolucionDTO.findAll(input,c1,c2);
		}else if(estado.equals("RECEPCIONADO")) {
			return devolucionDTO.findAll(input,c1,c2);
		}else {
			if(s_auth.equals("ROLE_SUULL_TIENDACENTRAL")) {
				Criteria c3 = where("estado").ne("ABIERTO");
				return devolucionDTO.findAll(input,c1,c3);
			}else {
				return devolucionDTO.findAll(input,c1);
			}
			
		}
		
    }
	@RequestMapping(value = "/alquileres_dto", method = RequestMethod.GET)
    public DataTablesOutput<Alquiler> listAlquileres(@Valid DataTablesInput input) {
        return repoAlquilerDTO.findAll(input);
    }
	@RequestMapping(value = "/alquileres_dto/{tipo}", method = RequestMethod.GET)
    public DataTablesOutput<Alquiler> listAlquileresFilter(@Valid DataTablesInput input,@PathVariable("tipo")String tipo) {
		
	
		if(tipo.equals("todos")) {
			Criteria c1 = where("estado").is("ACTIVO");
			return repoAlquilerDTO.findAll(input,c1);
		}else if(tipo.equals("entregas")) {
			Criteria c1 = where("operacion").is("ENTREGA");
			Criteria c2 = where("estado").is("ACTIVO");
			return repoAlquilerDTO.findAll(input,c1,c2);
		}else if(tipo.equals("devoluciones")) {
			
			Criteria c1 = where("operacion").is("DEVOLUCION");
			Criteria c2 = where("estado").is("ACTIVO");
			return repoAlquilerDTO.findAll(input,c1,c2);
		}else if(tipo.equals("anulados")) {
			Criteria c1 = where("estado").is("ANULADO");
			return repoAlquilerDTO.findAll(input,c1);
		}else {
			return repoAlquilerDTO.findAll(input);
		}
		
    }
	
	
	
	
	@RequestMapping(value = "/recepciones_dto", method = RequestMethod.GET)
    public DataTablesOutput<Recepcion> listRecepciones(@Valid DataTablesInput input) {
        return recepcionDTO.findAll(input);
    }
	@RequestMapping(value = "/transportes_dto", method = RequestMethod.GET)
    public DataTablesOutput<Transporte> listTransporte(@Valid DataTablesInput input) {
        return transporteDTO.findAll(input);
    }
	@RequestMapping(value = "/tiendas_dto", method = RequestMethod.GET)
    public DataTablesOutput<Tienda> listTienda(@Valid DataTablesInput input) {
        return tiendaDTO.findAll(input);
    }
	@RequestMapping(value = "/tiendas_global_stock_dto", method = RequestMethod.GET)
    public DataTablesOutput<Tienda> listTiendaGlobal(@Valid DataTablesInput input) {
        return tiendaDTO.findAll(input);
    }
	
	
	@RequestMapping(value = "/activos_dto", method = RequestMethod.GET)
    public DataTablesOutput<Activo> listActivo(@Valid DataTablesInput input) {
        return activoDTO.findAll(input);
    }
	
	@RequestMapping(value = "/usuarios_dto", method = RequestMethod.GET)
    public DataTablesOutput<Users> listUsuario(@Valid DataTablesInput input) {
        return usersDTO.findAll(input);
    }
	
	@RequestMapping(value = "/clientes_dto", method = RequestMethod.GET)
    public DataTablesOutput<Cliente> listClientes(@Valid DataTablesInput input) {
        return repoClienteDTO.findAll(input);
    }
	@RequestMapping(value = "/compras_dto", method = RequestMethod.GET)
    public DataTablesOutput<Compra> listCompras(@Valid DataTablesInput input) {
        return repoCompraDTO.findAll(input);
    }
	@RequestMapping(value = "/proveedores_dto", method = RequestMethod.GET)
    public DataTablesOutput<Proveedor> listProveedores(@Valid DataTablesInput input) {
        return repoProveedorDTO.findAll(input);
    } 
	@RequestMapping(value = "/proveedores_activos_dto", method = RequestMethod.GET)
    public DataTablesOutput<ProveedorActivo> listProveedoresActivos(@Valid DataTablesInput input) {
        return repoProveedorActivoDTO.findAll(input);
    } 
	@RequestMapping(value = "/reporte_proveedor_dto", method = RequestMethod.GET)
    public DataTablesOutput<ReporteProveedor> reporteListProveedores(@Valid DataTablesInput input) {
        return repoReporteProveedorDTO.findAll(input);
    }
	@RequestMapping(value = "/process_reporte", method = RequestMethod.GET)
    public CustomResponse processReporte(@RequestParam String fecha_inicio,@RequestParam String fecha_fin) {
		CustomResponse response = new CustomResponse();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = new Date();
		try {
			startDate = dateFormat.parse(fecha_inicio);
		} catch (ParseException e) {
			response.setStatus("ERROR");
			return response;
		} 
		Date endDate = new Date();
		try {
			endDate = dateFormat.parse(fecha_fin);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  

		repoReporteMovimientos.deleteAll();
		sequenceGenerator.resetSequence(ReporteMovimientos.SEQUENCE_NAME);
		
		List<Operacion> operaciones =  repoOperacion.findByFechaRegistroBetween(startDate,endDate);
		List<Devolucion> devoluciones =  repoDevolucion.findByFechaRegistroBetween(startDate,endDate);
    	
		if(operaciones.size()==0) {
			response.setStatus("NO_HAY_EXISTENCIAS");
			return response;
		}
		
		
    	for(int i=0;i<operaciones.size();i++) {
    		Operacion op = operaciones.get(i);
    		//ReporteMovimientos movimiento = getReporteMovFromOp(op);
    		//repoReporteMovimientos.save(movimiento);
    	}
    	for(int i=0;i<devoluciones.size();i++) {
    		Devolucion op = devoluciones.get(i);
    		//ReporteMovimientos movimiento = getReporteMovFromOp(op);
    		//repoReporteMovimientos.save(movimiento);
    	}
		
		response.setStatus("OK");
		
		return response;
	}
	
	ReporteMovimientos getReporteMovFromOp(Op op) {
		ReporteMovimientos movimiento = new ReporteMovimientos();
		movimiento.setNro_operacion(op.getId());
		movimiento.setTipo_movimiento("Despacho");
		
		movimiento.setOrigen_loc(op.getLocal_partida().getNumero()+"-"+op.getLocal_partida().getNombre());
		movimiento.setDestino_loc(op.getLocal_entrega().getNumero()+"-"+op.getLocal_entrega().getNombre());
		movimiento.setSku("PLMD");
		
		movimiento.setObservacion(op.getObservaciones());
		
		Transporte transporte = op.getTransporte();
		movimiento.setRuc_emptransp(transporte.getRuc());
		movimiento.setRazon_social(transporte.getRazon_social());
		movimiento.setNro_licencia(transporte.getLicencia());
		movimiento.setNom_chofer(transporte.getChofer());
		movimiento.setNro_placa(transporte.getPlaca());
		movimiento.setMarca(transporte.getMarca());
		movimiento.setNro_guia_remision(op.getGuia_remision());
		movimiento.setUsuario(op.getUsuario());
		
		
		movimiento.setFecha_emision(op.getFecha_registro());
		if(op.getEstado().equals("RECEPCIONADO")) {
			movimiento.setFecha_recepcion(op.getFecha_recepcion());
			movimiento.setTipo_movimiento_final("Recepcion");
		}else {
			movimiento.setFecha_recepcion(null);
		}
		return movimiento;
	}
	
	@RequestMapping(value = "/process_rebate", method = RequestMethod.GET)
    public Rebate processRebateGet(@RequestParam String fecha_inicio,@RequestParam String fecha_fin) {
		
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = new Date();
		try {
			startDate = dateFormat.parse(fecha_inicio);
		} catch (ParseException e) {
			return new Rebate();
		} 
		Date endDate = new Date();
		try {
			endDate = dateFormat.parse(fecha_fin);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		Rebate reb = new Rebate();
		reb.setId("1"); 
		repoReporteProveedor.deleteAll();
		sequenceGenerator.resetSequence(ReporteProveedor.SEQUENCE_NAME);
		
		ArrayList<Proveedor> proveedores = (ArrayList<Proveedor>) this.repoProveedor.findAll();
		
		for(int h=0;h<proveedores.size();h++) {
			Proveedor prov = proveedores.get(h);
			
			
			List<Activo> activos = this.repoActivo.findAll();
			ArrayList<RespCreateMonto> montos = new ArrayList<RespCreateMonto>();
			boolean find_monto = false;
			String usuario = "";
			for(int i=0;i<activos.size();i++) {
				Activo activo = activos.get(i);
				ArrayList<Movimiento> movs = (ArrayList<Movimiento>) repoMovimiento.findByProveedorAndActivo(prov.getId(), activo.getId());
				if(movs.size()>0) {
					RespCreateMonto resp = getMontoBySKU(startDate,endDate,activo,prov);
					if(resp.monto>0) {
						if(!resp.usuario.equals("")) {
							usuario=resp.usuario;
						}
						if(resp.monto>0) {
							find_monto = true;
						}
						montos.add(resp);
					}
				}
			}

			if(find_monto) {
				ReporteProveedor r = new ReporteProveedor();
				r.setId(sequenceGenerator.generateSequence(ReporteProveedor.SEQUENCE_NAME)+"");
				r.setRuc_proveedor(prov.getRuc());
				r.setNombre_proveedor(prov.getRazon_social());
				
				r.setMontos(montos);
				r.setUsuario(usuario);
				r.setFecha_inicio(startDate);
				r.setFecha_fin(endDate);
				
				repoReporteProveedor.save(r);
			}
		}
        return reb;
    }
	
	public int daysBetween(Date d1, Date d2){
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}
	
	private RespCreateMonto getMontoBySKU(Date startDate,Date endDate,Activo activo,Proveedor prov) {
		ArrayList<Movimiento> movimientos = (ArrayList<Movimiento>) repoMovimiento.findByProveedorAndActivoAndFechaBetween(prov.getId(), activo.getId(), startDate, endDate);
		long diff = endDate.getTime() - startDate.getTime();
	    Integer dias = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	    DateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd");
	    Integer stock = 0;
	    Double costo_total = 0.0;
	    Date currentDate = startDate;
	    String usuario = "";
	    
	    ArrayList<Movimiento> ult_mov = (ArrayList<Movimiento>) repoMovimiento.findByProveedorAndActivoAndFecha(prov.getId(), activo.getId(), startDate);
	    for(int p=0;p<ult_mov.size();p++) {
	    	Movimiento mov =ult_mov.get(p);
	    	stock = mov.getStock();
	    	break;
	    }
	    
	    Double last_precio=0.0;
	    Double _alpha=0.0;
	    
	    Log("---------------------");
	    for(int p=0;p<movimientos.size();p++) {
	    	Movimiento mov =movimientos.get(p);
	    	int _dias = daysBetween(mov.getFecha(),endDate);
	    	Double _p = mov.getCantidad()*mov.getPrecio()*_dias;
	    	costo_total+=_p;
	    }
	    Log("---------------------");
	    RespCreateMonto resp = new RespCreateMonto();
	    resp.monto = costo_total;
	    resp.usuario = usuario;
	    resp.activo = activo;
	   // Log("resp.usuario "+resp.usuario);
	    return resp;
	}

	
	public boolean compareDates(String psDate1, String psDate2) throws ParseException{
        SimpleDateFormat dateFormat = new SimpleDateFormat ("dd/MM/yyyy");
        Date date1 = dateFormat.parse(psDate1);
        Date date2 = dateFormat.parse(psDate2);
        if(date2.after(date1)) {
            return true;
        } else {
            return false;
        }
    }
	
	@RequestMapping(value = "/activos", method = RequestMethod.GET)
	public List<ActivoRef> getActivos() {
		
		Tienda tienda = getCurrentTienda();
		ArrayList<ActivoRef> activosRef = new ArrayList<ActivoRef>();
		List<Activo> activos = repoActivo.findAll();
		for(int i=0;i<activos.size();i++) {
			Activo activo = activos.get(i);
			
			ActivoRef ar = new ActivoRef();
			ar.setId(activo.getId());
			ar.setSku(activo.getSku());
			ar.setNombre(activo.getNombre());
			ar.setStock(tienda.getStockBySKU(activo.getSku()));
			activosRef.add(ar);
		}
		
		return activosRef;
	}
	@RequestMapping(value = "/activo/{key}", method = RequestMethod.GET)
	public Activo getActivo(@PathVariable("key")String id) {
		return (Activo) repoActivo.findById(id).get();
	}
	
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public CustomResponse test() {
		Users user = new Users();
		//user.setPassword(bCryptPasswordEncoder.encode("admin"));
        Role userRole = roleRepository.findByRole("ADMIN");
        //user.setRoles(new HashSet<>(Arrays.asList(userRole)));
        repoUsers.save(user);
        
        CustomResponse response = new CustomResponse();
        response.setResponse("OK");
        return response;
	}
	

	private Tienda getCurrentTienda() {
		DefaultOidcUser user = (DefaultOidcUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return repoUsers.findById(user.getAttribute("oid")).get().getTienda();
	}
	private void Log(String log) {
		System.out.print(log+"\n");
	}
}

class DateUtil
{
    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }
}
class CustomResponse{
	
	private String response;
	private String status;
	
	public CustomResponse() {
		
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
