package com.tottus.suull.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tottus.suull.entity.Activo;
import com.tottus.suull.entity.Devolucion;
import com.tottus.suull.entity.Tienda;
import com.tottus.suull.entity.Transporte;
import com.tottus.suull.entity.Users;
import com.tottus.suull.repository.RepoActivo;
import com.tottus.suull.repository.RepoDevolucion;
import com.tottus.suull.repository.RepoOperacion;
import com.tottus.suull.repository.RepoTienda;
import com.tottus.suull.repository.RepoTransporte;
import com.tottus.suull.repository.RepoUsers;
import com.tottus.suull.service.SequenceGeneratorService;


@Controller
@RequestMapping("/devolucion")
public class DevolucionesController {
	
	@Autowired
	private RepoOperacion repoOperacion;
	
	@Autowired
	private RepoDevolucion repoDevolucion;
	
	@Autowired
	private RepoTransporte repoTransporte;
	
	@Autowired
	private RepoActivo repoActivo;
	
	@Autowired
	private RepoTienda repoTienda;
	
	@Autowired
	private RepoUsers repoUsers;
	
	final Logger logger = LoggerFactory.getLogger(DevolucionesController.class);
	
	private SequenceGeneratorService sequenceGenerator;
	
	 @Autowired
	    public DevolucionesController(SequenceGeneratorService sequenceGeneratorService) {
	        this.sequenceGenerator = sequenceGeneratorService;
	    }
	
	 private Date getLocalDate(Date fecha) {
			int year = 1900+fecha.getYear();
			int date = fecha.getDate();
			int month = fecha.getMonth()+1;
			return java.util.Date.from(  LocalDate.of( year , month , date ).atStartOfDay(ZoneId.of( "America/Lima" )).toInstant());
	}
	 
	@GetMapping("/crear")
	public String operacionForm(Map<String, Object> model) {
		
		Devolucion dev = new Devolucion();
		dev.setFecha_inicio_traslado(getLocalDate(new Date()));

		model.put("devolucion", dev);
		model.put("menu", "devolucion");
		model.put("view_mode",false);
		
		model.put("tienda_origen",getCurrentTienda().getNombre());
		
		model = operacionCreateSettings(model);
		model.put("titulo","Nueva Devolución");
		return "devolucion";

	}
	
	private String saveOperacion(Map<String, Object> model, @Valid Devolucion operacion, BindingResult result,String estado) {
		if (result.hasErrors()) {
			List<ObjectError> errores = result.getAllErrors();
			for (int i = 0; i < errores.size(); i++) {
				System.out.println(errores.get(i).getDefaultMessage());
			}
			model = operacionCreateSettings(model);
			return "devolucion";
		}
		operacion.setFecha_inicio_traslado(getLocalDate(operacion.getFecha_inicio_traslado()));
		operacion.setNombre_local_partida(getCurrentTienda().getNombre());
		operacion.setNombre_local_entrega(operacion.getLocal_entrega().getNombre());
		
		operacion.setUsuario(this.getCurrentUser());
		operacion.setLocal_partida(this.getCurrentTienda());
		operacion.setLocal_entrega_id(operacion.getLocal_entrega().getId());
		operacion.setLocal_partida_id(operacion.getLocal_partida().getId());
		
		System.out.print("operacion.getTransferencia_pura() "+operacion.getTransferencia_pura());
		if(operacion.getTransferencia_pura()==null) {
			operacion.setTransferencia_pura(false);
		}else {
			operacion.setTransferencia_pura(operacion.getTransferencia_pura());
		}
		
		String id = operacion.getId();
		System.out.print("id = "+id);
		if (id.equals("")) {
			operacion.setId(sequenceGenerator.generateSequence(Devolucion.SEQUENCE_NAME)+"");
			model.put("mensaje", "Devolución creada con exito");
			operacion.setEstado(estado);
			operacion.setFecha_registro(new Date());
			operacion.setFecha_actualizacion(new Date());
			
		} else {
			Devolucion op = repoDevolucion.findById(operacion.getId()).get();
			op.setActivos(operacion.getActivos());
			op.setTransferencia_pura(operacion.getTransferencia_pura());
			op.setFecha_actualizacion(new Date());
			if(estado.equals("CERRADO")) {
				op.setEstado("CERRADO");
			}
			operacion = op;
			model.put("mensaje", "Devolución actualizada con exito");
		}
		
		Integer cantidad = 0;
		for(int i=0;i<operacion.getActivos().size();i++) {
			cantidad+= operacion.getActivos().get(i).getCantidad();
		}
		
		operacion.setCantidad_activos(cantidad);
		
		repoDevolucion.save(operacion);
		model.put("menu", "devolucion");
		model.put("view_mode",false);
		model.put("nro_operacion", operacion.getNro_operacion());
		model.put("create", false);
		
		return "devolucion";
	}

	@PostMapping(value = "/guardar", params = {"cerrar_devolucion"})
	public String operacionSubmitCerrar(Map<String, Object> model, @Valid Devolucion operacion, BindingResult result) {
		return saveOperacion(model,operacion,result,"CERRADO");
	}
	
	@PostMapping("/guardar")
	public String operacionSubmit(Map<String, Object> model, @Valid Devolucion operacion, BindingResult result) {
		return saveOperacion(model,operacion,result,"ABIERTO");
	}
	
	@GetMapping("/editar/{id}")
	public String operacionEditar(Model model, @PathVariable("id") String id) {
		Devolucion t = repoDevolucion.findById(id).get();
		model.addAttribute("nro_operacion", t.getNro_operacion());
		List<Tienda> tiendas = this.repoTienda.findAll();
		model.addAttribute("tiendas", tiendas);
		List<Transporte> transportes = this.repoTransporte.findAll();
		model.addAttribute("transportes", transportes);
		List<Activo> activos = this.repoActivo.findAll();
		model.addAttribute("activos", activos);
		Integer total = 0;
		for(int i=0;i<t.getActivos().size();i++) {
			total += t.getActivos().get(i).getCantidad();
		}
		model.addAttribute("transferencia_pura",t.getTransferencia_pura());
		model.addAttribute("view_mode",false);
		model.addAttribute("total_activos", total);
		model.addAttribute("menu", "devolucion");
		model.addAttribute("create", true);
		model.addAttribute("devolucion", t);
		model.addAttribute("estado",t.getEstado());
		model.addAttribute("observacion_tienda",t.getObservacion_tienda());
		model.addAttribute("titulo","Editar Devolución");
		
		return "devolucion";
	}
	@GetMapping("/ver/{id}")
	public String operacionVer(Model model, @PathVariable("id") String id) {
		Devolucion t = repoDevolucion.findById(id).get();
		model.addAttribute("nro_operacion", t.getNro_operacion());
		List<Tienda> tiendas = this.repoTienda.findAll();
		model.addAttribute("tiendas", tiendas);
		List<Transporte> transportes = this.repoTransporte.findAll();
		model.addAttribute("transportes", transportes);
		List<Activo> activos = this.repoActivo.findAll();
		model.addAttribute("transferencia_pura",t.getTransferencia_pura());
		model.addAttribute("activos", activos);
		model.addAttribute("view_mode",true);
		Integer total = 0;
		for(int i=0;i<t.getActivos().size();i++) {
			total += t.getActivos().get(i).getCantidad();
		}
		
		model.addAttribute("tienda_origen",t.getLocal_partida().getNombre());
		model.addAttribute("total_activos", total);
		model.addAttribute("menu", "devolucion");
		model.addAttribute("create", true);
		model.addAttribute("devolucion", t);
		model.addAttribute("titulo","Devolucion");
		return "devolucion";
	}
	
	@GetMapping("/eliminar/{id}")
	public String operacionEliminar(Model model, @PathVariable("id") String id) {
		model.addAttribute("menu", "devolucion");
		repoDevolucion.deleteById(id);
		return "redirect:/devolucion/listar";
	}
	@GetMapping("/cerrar/{id}")
	public String operacionCerrar(Model model, @PathVariable("id") String id) {
		
		Devolucion t = repoDevolucion.findById(id).get();
		t.setEstado("CERRADO");
		repoDevolucion.save(t);
		model.addAttribute("menu", "devolucion");
		return "redirect:/devolucion/listar";
	}
	@GetMapping("/abrir/{id}")
	public String operacionAbrir(Model model, @PathVariable("id") String id) {
		Devolucion t = repoDevolucion.findById(id).get();
		t.setEstado("ABIERTO");
		repoDevolucion.save(t);
		model.addAttribute("menu", "devolucion");
		return "redirect:/devolucion/listar";
	}
	
	private String getNroOperacion() {
		return "E"+repoDevolucion.findAll().size();
	}

	Map<String, Object> operacionCreateSettings(Map<String, Object> model) {
		model.put("create", true);
		model.put("nro_operacion", getNroOperacion());
		List<Tienda> tiendas_db = this.repoTienda.findAll();
		
		Tienda currentTienda = this.getCurrentTienda();
		
		logger.info("Tienda Nombre "+currentTienda.getNombre());
		logger.info("Tienda Numero "+currentTienda.getNumero());
		//currentTienda
		
		ArrayList<Tienda> tiendas = new ArrayList<Tienda>();
		for(int i=0;i<tiendas_db.size();i++) {
			Tienda tnd = tiendas_db.get(i);
			if(!tnd.getId().equals(currentTienda.getId()) && tnd.getNumero()!=1000) {
				tiendas.add(tnd);
			}
		}
		
		
		model.put("tiendas", tiendas);
		List<Transporte> transportes = this.repoTransporte.findAll();
		model.put("transportes", transportes);
		List<Activo> activos_db = this.repoActivo.findAll();
		
		
		
		ArrayList<Activo> activos = new ArrayList<Activo>();
		for(int i=0;i<activos_db.size();i++) {
			Activo activo = activos_db.get(i);
			String nombre = activo.getNombre() + " ("+currentTienda.getStockBySKU(activo.getSku())+")";
			activo.setNombre(nombre);
			activos.add(activo);
		}
		
		model.put("activos", activos);
		model.put("total_activos", 0);
		model.put("menu", "devolucion");
		return model;
	}

	private Model getInfoOperacion(Model model,Integer tipo){
		
		String currentStoreId = getCurrentTienda().getId();
		
		Integer abiertas = repoDevolucion.findByEstadoAndLocalpartidaid("ABIERTO",currentStoreId).size();
		Integer cerradas = repoDevolucion.findByEstadoAndLocalpartidaid("CERRADO",currentStoreId).size();
		Integer observadas = repoDevolucion.findByEstadoAndLocalpartidaid("OBSERVADO",currentStoreId).size();
		Integer recepcionadas = repoDevolucion.findByEstadoAndLocalpartidaid("RECEPCIONADO",currentStoreId).size();

		model.addAttribute("abiertas",abiertas);
		model.addAttribute("cerradas",cerradas);
		model.addAttribute("observadas",observadas);
		model.addAttribute("recepcionadas",recepcionadas);
		
		model.addAttribute("menu", "devolucion");
		if(tipo==-1) {
			model.addAttribute("estado","TODOS");
		}
		if(tipo==0) {
			model.addAttribute("estado","ABIERTO");
		}
		if(tipo==1) {
			model.addAttribute("estado","CERRADO");
		}
		if(tipo==2) {
			model.addAttribute("estado","OBSERVADO");
		}
		if(tipo==3) {
			model.addAttribute("estado","RECEPCIONADO");
		}
		model.addAttribute("tipo","devolucion");
		
		
		
		return model;
	}
	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public String listOperaciones(Model model) {
		model = getInfoOperacion(model,-1);
		model.addAttribute("tipo","devolucion");
		return "devoluciones";
	}
	
	@RequestMapping(value = "/listar/{tipo}", method = RequestMethod.GET)
	public String listOperacionesTipo(Model model,@PathVariable("tipo") Integer tipo) {
		model = getInfoOperacion(model,tipo);
		model.addAttribute("tipo","devolucion");
		return "devoluciones";
	}
	private Tienda getCurrentTienda() {
		DefaultOidcUser user = (DefaultOidcUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return repoUsers.findById(user.getAttribute("oid")).get().getTienda();
	}
	private Users getCurrentUser() {
		DefaultOidcUser user = (DefaultOidcUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return repoUsers.findById(user.getAttribute("oid")).get();
	}
}
