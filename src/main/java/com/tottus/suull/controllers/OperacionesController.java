package com.tottus.suull.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tottus.suull.entity.Activo;
import com.tottus.suull.entity.ActivoDetalle;
import com.tottus.suull.entity.Operacion;
import com.tottus.suull.entity.Tienda;
import com.tottus.suull.entity.Transporte;
import com.tottus.suull.entity.Users;
import com.tottus.suull.repository.RepoActivo;
import com.tottus.suull.repository.RepoDevolucion;
import com.tottus.suull.repository.RepoOperacion;
import com.tottus.suull.repository.RepoTienda;
import com.tottus.suull.repository.RepoTransporte;
import com.tottus.suull.repository.RepoUsers;
import com.tottus.suull.service.SequenceGeneratorService;


@Controller
@RequestMapping("/despacho")
public class OperacionesController {
	
	@Autowired
	private RepoOperacion repoOperacion;
	
	@Autowired
	private RepoDevolucion repoDevolucion;
	
	@Autowired
	private RepoTransporte repoTransporte;
	
	@Autowired
	private RepoActivo repoActivo;
	
	@Autowired
	private RepoTienda repoTienda;
	
	@Autowired
	private RepoUsers repoUsers;
	
	final Logger logger = LoggerFactory.getLogger(OperacionesController.class);
	
	private SequenceGeneratorService sequenceGenerator;
	
	 @Autowired
	    public OperacionesController(SequenceGeneratorService sequenceGeneratorService) {
	        this.sequenceGenerator = sequenceGeneratorService;
	    }
	
	private Date getLocalDate(Date fecha) {
		int year = 1900+fecha.getYear();
		int date = fecha.getDate();
		int month = fecha.getMonth()+1;
		return java.util.Date.from(  LocalDate.of( year , month , date ).atStartOfDay(ZoneId.of( "America/Lima" )).toInstant());
	}
	@GetMapping("/crear")
	public String operacionForm(Map<String, Object> model) {
		
		Operacion op = new Operacion();
		
		op.setFecha_inicio_traslado(getLocalDate(new Date()));
		
		model.put("despacho", op);
		model.put("menu", "despacho");
		model.put("view_mode",false);
		
		model.put("tienda_origen",getCurrentTienda().getNombre());
		
		model = operacionCreateSettings(model);
		model.put("titulo","Nuevo Despacho");
		return "despacho";

	}
	
	
	private String saveOperacion(Map<String, Object> model, @Valid Operacion operacion, BindingResult result,String estado) {
		if (result.hasErrors()) {
			List<ObjectError> errores = result.getAllErrors();
			for (int i = 0; i < errores.size(); i++) {
				System.out.println(errores.get(i).getDefaultMessage());
			}
			model = operacionCreateSettings(model);
			return "despacho";
		}

		operacion.setFecha_inicio_traslado(getLocalDate(operacion.getFecha_inicio_traslado()));
		operacion.setNombre_local_partida(getCurrentTienda().getNombre());
		operacion.setNombre_local_entrega(operacion.getLocal_entrega().getNombre());

		operacion.setUsuario(this.getCurrentUser());
		operacion.setLocal_partida(this.getCurrentTienda());
		operacion.setLocal_entrega_id(operacion.getLocal_entrega().getId());
		operacion.setLocal_partida_id(operacion.getLocal_partida().getId());
		String id = operacion.getId();
		System.out.print("id = "+id);
		if (id.equals("")) {
			operacion.setId(sequenceGenerator.generateSequence(Operacion.SEQUENCE_NAME)+"");
			model.put("mensaje", "Despacho creado con exito");
			operacion.setEstado(estado);
			operacion.setFecha_registro(new Date());
			operacion.setFecha_actualizacion(new Date());
			if(estado.equals("CERRADO")) {
				System.out.print("ACTIVOS TOTAL "+operacion.getActivos().size());
				activosATienda(operacion.getActivos(),this.getCurrentTienda(),operacion.getLocal_entrega());
			}
			
		} else {
			Operacion op = repoOperacion.findById(operacion.getId()).get();
			op.setActivos(operacion.getActivos());
			op.setFecha_actualizacion(new Date());
			if(estado.equals("CERRADO")) {
				op.setEstado("CERRADO");
				
				activosATienda(operacion.getActivos(),this.getCurrentTienda(),operacion.getLocal_entrega());
			}
			operacion = op;
			model.put("mensaje", "Despacho actualizado con exito");
		}
		Integer cantidad = 0;
		for(int i=0;i<operacion.getActivos().size();i++) {
			//String SKU =  operacion.getActivos().get(i).getSku();
			//Activo activo = this.repoActivo.findBySku(SKU);
			//Integer stock = activo.getStock() - operacion.getActivos().get(i).getCantidad();
			//if(stock>=0) {
				//activo.setStock(stock);
				//repoActivo.save(activo);
			//}
			cantidad+= operacion.getActivos().get(i).getCantidad();
		}
		operacion.setCantidad_activos(cantidad);
		repoOperacion.save(operacion);
		model.put("menu", "despacho");
		model.put("view_mode",false);
		model.put("nro_operacion", operacion.getNro_operacion());
		model.put("create", false);
		return "despacho";
	}
	@PostMapping(value = "/guardar", params = {"cerrar_despacho"})
	public String operacionSubmitCerrar(Map<String, Object> model, @Valid Operacion operacion, BindingResult result) {
		return saveOperacion(model,operacion,result,"CERRADO");
	}
	
	@PostMapping("/guardar")
	public String operacionSubmit(Map<String, Object> model, @Valid Operacion operacion, BindingResult result) {
		return saveOperacion(model,operacion,result,"ABIERTO");
	}
	
	@GetMapping("/editar/{id}")
	public String operacionEditar(Model model, @PathVariable("id") String id) {
		Operacion t = repoOperacion.findById(id).get();
		model.addAttribute("nro_operacion", t.getNro_operacion());
		List<Tienda> tiendas = this.repoTienda.findAll();
		model.addAttribute("tiendas", tiendas);
		List<Transporte> transportes = this.repoTransporte.findAll();
		model.addAttribute("transportes", transportes);
		List<Activo> activos = this.repoActivo.findAll();
		model.addAttribute("activos", activos);
		Integer total = 0;
		for(int i=0;i<t.getActivos().size();i++) {
			total += t.getActivos().get(i).getCantidad();
		}
		model.addAttribute("view_mode",false);
		model.addAttribute("total_activos", total);
		model.addAttribute("menu", "despacho");
		model.addAttribute("create", true);
		model.addAttribute("despacho", t);
		model.addAttribute("estado",t.getEstado());
		model.addAttribute("observacion_tienda",t.getObservacion_tienda());
		model.addAttribute("titulo","Editar Despacho");
		
		return "despacho";
	}
	@GetMapping("/ver/{id}")
	public String operacionVer(Model model, @PathVariable("id") String id) {
		Operacion t = repoOperacion.findById(id).get();
		model.addAttribute("nro_operacion", t.getNro_operacion());
		List<Tienda> tiendas = this.repoTienda.findAll();
		model.addAttribute("tiendas", tiendas);
		List<Transporte> transportes = this.repoTransporte.findAll();
		model.addAttribute("transportes", transportes);
		List<Activo> activos = this.repoActivo.findAll();
		model.addAttribute("activos", activos);
		model.addAttribute("view_mode",true);
		Integer total = 0;
		for(int i=0;i<t.getActivos().size();i++) {
			total += t.getActivos().get(i).getCantidad();
		}
		model.addAttribute("tienda_origen",t.getLocal_partida().getNombre());
		model.addAttribute("total_activos", total);
		model.addAttribute("menu", "despacho");
		model.addAttribute("create", true);
		model.addAttribute("despacho", t);
		model.addAttribute("titulo","Despacho");
		return "despacho";
	}
	
	@GetMapping("/eliminar/{id}")
	public String operacionEliminar(Model model, @PathVariable("id") String id) {
		model.addAttribute("menu", "despacho");
		repoOperacion.deleteById(id);
		return "redirect:/despacho/listar";
	}
	@GetMapping("/cerrar/{id}")
	public String operacionCerrar(Model model, @PathVariable("id") String id) {
		
		Operacion t = repoOperacion.findById(id).get();
		t.setEstado("CERRADO");
		activosATienda(t.getActivos(),t.getLocal_partida(),t.getLocal_entrega());
		repoOperacion.save(t);
		model.addAttribute("menu", "despacho");
		return "redirect:/despacho/listar";
	}
	@GetMapping("/abrir/{id}")
	public String operacionAbrir(Model model, @PathVariable("id") String id) {
		Operacion t = repoOperacion.findById(id).get();
		t.setEstado("ABIERTO");
		activosATienda(t.getActivos(),t.getLocal_entrega(),t.getLocal_partida());
		repoOperacion.save(t);
		model.addAttribute("menu", "despacho");
		return "redirect:/despacho/listar";
	}
	private boolean activosATienda(ArrayList<ActivoDetalle> activo_detalle,Tienda tienda_partida,Tienda tienda_entrega) {
		if(tienda_partida.getId().equals(tienda_entrega.getId())){
			return false;
		}
		System.out.print("\n---------ACTIVOS A TIENDA---------\n");
		System.out.print("---------DE "+tienda_partida.getNombre()+"---a---"+tienda_entrega.getNombre()+"----\n");
		for (int i = 0; i < activo_detalle.size(); i++) {
			ActivoDetalle ad = activo_detalle.get(i);
			Integer stock_partida = tienda_partida.getStockBySKU(ad.getSku());
			Integer s1 = stock_partida - ad.getCantidad();
			System.out.print("---------s1 "+s1+"---------\n");
			if(s1>=0)tienda_partida.setStockBySKU(ad.getSku(), s1);
			Integer stock_entrega = tienda_entrega.getStockBySKU(ad.getSku());
			Integer s2 = stock_entrega + ad.getCantidad();
			System.out.print("---------s2 "+s2+"---------\n");
			if(s2>=0)tienda_entrega.setStockBySKU(ad.getSku(), s2);
		}
		this.repoTienda.save(tienda_partida);
		this.repoTienda.save(tienda_entrega);
		tienda_partida.updateActivos();
		tienda_entrega.updateActivos();
		this.repoTienda.save(tienda_partida);
		this.repoTienda.save(tienda_entrega);
		
		System.out.print("---------------------------\n");
		return true;
	}
	private String getNroOperacion() {
		return "D"+repoDevolucion.findAll().size();
	}

	Map<String, Object> operacionCreateSettings(Map<String, Object> model) {
		model.put("create", true);
		model.put("nro_operacion", getNroOperacion());
		List<Tienda> tiendas_db = this.repoTienda.findAll();
		
		ArrayList<Tienda> tiendas = new ArrayList<Tienda>();
		for(int i=0;i<tiendas_db.size();i++) {
			Tienda tnd = tiendas_db.get(i);
			if(!tnd.getId().equals(getCurrentTienda().getId()) && tnd.getNumero()!=1000) {
				tiendas.add(tnd);
			}
		}

		model.put("tiendas", tiendas);
		List<Transporte> transportes = this.repoTransporte.findAll();
		model.put("transportes", transportes);
		List<Activo> activos = this.repoActivo.findAll();
		model.put("activos", activos);
		model.put("total_activos", 0);
		model.put("menu", "despacho");
		return model;
	}
	/*
	private Model operacionesMod(Model model) {
		Integer abiertas = repoOperacion.findByEstado("ABIERTO").size();
		Integer cerradas = repoOperacion.findByEstado("CERRADO").size();
		Integer observadas = repoOperacion.findByEstado("OBSERVADO").size();
		Integer recepcionadas = repoOperacion.findByEstado("RECEPCIONADO").size();

		model.addAttribute("abiertas",abiertas);
		model.addAttribute("cerradas",cerradas);
		model.addAttribute("observadas",observadas);
		model.addAttribute("recepcionadas",recepcionadas);
		
		model.addAttribute("menu", "despacho");
		return model;
	}*/
	private Model getInfoOperacion(Model model,Integer tipo){
		
		Integer abiertas = repoOperacion.findByEstado("ABIERTO").size();
		Integer cerradas = repoOperacion.findByEstado("CERRADO").size();
		Integer observadas = repoOperacion.findByEstado("OBSERVADO").size();
		Integer recepcionadas = repoOperacion.findByEstado("RECEPCIONADO").size();

		model.addAttribute("abiertas",abiertas);
		model.addAttribute("cerradas",cerradas);
		model.addAttribute("observadas",observadas);
		model.addAttribute("recepcionadas",recepcionadas);
		
		model.addAttribute("menu", "despacho");
		if(tipo==-1) {
			model.addAttribute("estado","TODOS");
		}
		if(tipo==0) {
			model.addAttribute("estado","ABIERTO");
		}
		if(tipo==1) {
			model.addAttribute("estado","CERRADO");
		}
		if(tipo==2) {
			model.addAttribute("estado","OBSERVADO");
		}
		if(tipo==3) {
			model.addAttribute("estado","RECEPCIONADO");
		}
		model.addAttribute("tipo","despacho");
		
		
		
		return model;
	}
	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public String listOperaciones(Model model) {
		model = getInfoOperacion(model,-1);
		model.addAttribute("tipo","despacho");
		return "despachos";
	}
	
	@RequestMapping(value = "/listar/{tipo}", method = RequestMethod.GET)
	public String listOperacionesTipo(Model model,@PathVariable("tipo") Integer tipo) {
		model = getInfoOperacion(model,tipo);
		model.addAttribute("tipo","despacho");
		return "despachos";
	}
	private Tienda getCurrentTienda() {
		DefaultOidcUser user = (DefaultOidcUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return repoUsers.findById(user.getAttribute("oid")).get().getTienda();
	}
	private Users getCurrentUser() {
		DefaultOidcUser user = (DefaultOidcUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return repoUsers.findById(user.getAttribute("oid")).get();
	}
}
