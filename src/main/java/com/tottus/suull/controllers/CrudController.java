package com.tottus.suull.controllers;

import org.springframework.data.mongodb.datatables.DataTablesRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import lombok.Data;


@Controller
public class CrudController {
	
	public DataTablesRepository repo;
	public String singlename;
	public String pluralname;
	public String className;
	
	@GetMapping("/listar")
	public String listar(Model model) {
		return "mant/"+pluralname;
	}

	@GetMapping("/add")
	public String create(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", pluralname);
		model.addAttribute("create", true);
		
		Object xyz;
		try {
			xyz = Class.forName(className).newInstance();
			model.addAttribute(singlename,xyz);
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return "mant/"+singlename;
	}

	@PostMapping("/save")
	public String save(Model model, @ModelAttribute MongoRepository usuario) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", pluralname);
		model.addAttribute("create", false);
		repo.save(usuario);
		return "mant/"+singlename;
	}

	@GetMapping("/edit/{id}")
	public String edit(Model model, @PathVariable("id") String id) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", pluralname);
		MongoRepository t = (MongoRepository) repo.findById(id).get();
		model.addAttribute("create", true);
		model.addAttribute(singlename, t);
		return "mant/"+singlename;
	}
}
