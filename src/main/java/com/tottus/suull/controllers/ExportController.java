package com.tottus.suull.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.tottus.suull.entity.Devolucion;
import com.tottus.suull.entity.Operacion;
import com.tottus.suull.entity.ReporteMovimientos;
import com.tottus.suull.entity.ReporteProveedor;
import com.tottus.suull.entity.Tienda;
import com.tottus.suull.entity.Transporte;
import com.tottus.suull.repository.RepoActivo;
import com.tottus.suull.repository.RepoDevolucion;
import com.tottus.suull.repository.RepoOperacion;
import com.tottus.suull.repository.RepoReporteMovimientos;
import com.tottus.suull.repository.RepoReporteProveedor;
import com.tottus.suull.repository.RepoTienda;
import com.tottus.suull.service.PDFView;
import com.tottus.suull.service.XLSView;

@Controller
public class ExportController {
    
	@PersistenceContext private EntityManager entityManager;
    @Autowired private RepoReporteProveedor repoReporteProveedor;
    
    @Autowired private RepoTienda repoTienda;
    @Autowired private RepoActivo repoActivos;
    @Autowired private RepoOperacion repoOperacion;
    @Autowired private RepoDevolucion repoDevolucion;
    @Autowired private RepoReporteMovimientos repoReporteMovimientos;
    
    
    @RequestMapping("/rebate.xls")
    public XLSView listAsXls(Model model) {
    	List<ReporteProveedor> data = repoReporteProveedor.findAll();
        model.addAttribute("data", data);
        model.addAttribute("reporte", 1);
        return new XLSView();  // Return view explicitly
    }
    
    @RequestMapping("/movimientos.xls")
    public XLSView movimientosXls(Model model) {
    	List<ReporteMovimientos> data = repoReporteMovimientos.findAll();
    	/*List<Operacion> operaciones =  repoOperacion.findAll();
    	ArrayList<ReporteMovimientos> data = new ArrayList<ReporteMovimientos>();
    	for(int i=0;i<operaciones.size();i++) {
    		Operacion op = operaciones.get(i);
    		
    		ReporteMovimientos movimiento = new ReporteMovimientos();
    		movimiento.setNro_operacion(op.getId());
    		movimiento.setTipo_movimiento("Despacho");
    		
    		movimiento.setOrigen_loc(op.getLocal_partida().getNumero()+"-"+op.getLocal_partida().getNombre());
    		movimiento.setDestino_loc(op.getLocal_entrega().getNumero()+"-"+op.getLocal_entrega().getNombre());
    		movimiento.setSku("PLMD");
    		
    		movimiento.setObservacion(op.getObservaciones());
    		
    		Transporte transporte = op.getTransporte();
    		movimiento.setRuc_emptransp(transporte.getRuc());
    		movimiento.setRazon_social(transporte.getRazon_social());
    		movimiento.setNro_licencia(transporte.getLicencia());
    		movimiento.setNom_chofer(transporte.getChofer());
    		movimiento.setNro_placa(transporte.getPlaca());
    		
    		movimiento.setNro_guia_remision(op.getGuia_remision());
    		movimiento.setUsuario(op.getUsuario());
    		
    		
    		movimiento.setFecha_emision(op.getFecha_registro());
    		if(op.getEstado().equals("RECEPCIONADO")) {
    			movimiento.setFecha_recepcion(op.getFecha_recepcion());
    			movimiento.setTipo_movimiento_final("Recepcion");
    		}else {
    			movimiento.setFecha_recepcion(null);
    		}
    		
    		data.add(movimiento);
    	}*/
  
        model.addAttribute("data", data);
        model.addAttribute("reporte", 3);
        return new XLSView();  // Return view explicitly
    }
    
    @RequestMapping("/stock_general.xls")
    public XLSView stockGeneralXls(Model model) {
    	model.addAttribute("activos", repoActivos.findAll());
        model.addAttribute("data", repoTienda.findAll());
        model.addAttribute("reporte", 2);
        return new XLSView();
    }
    
    
    
    @RequestMapping("/devolucion/guia.pdf/{id}")
    public PDFView getDevGuia(Model model, @PathVariable("id") String id) {
    	//List<ReporteProveedor> data = repoReporteProveedor.findAll();
        
    	Devolucion devolucion = repoDevolucion.findById(id).get();
    	
    	model.addAttribute("devolucion", devolucion);
        return new PDFView();  // Return view explicitly
    }
    @RequestMapping("/despacho/guia.pdf/{id}")
    public PDFView getDesGuia(Model model, @PathVariable("id") String id) {
    	//List<ReporteProveedor> data = repoReporteProveedor.findAll();
        
    	Operacion operacion = repoOperacion.findById(id).get();
    	
    	model.addAttribute("operacion", operacion);
        return new PDFView();  // Return view explicitly
    }
    
    

}
