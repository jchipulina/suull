package com.tottus.suull.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tottus.suull.entity.Activo;
import com.tottus.suull.entity.ActivoDetalle;
import com.tottus.suull.entity.ActivoDetalleProveedor;
import com.tottus.suull.entity.ActivoRef;
import com.tottus.suull.entity.Alquiler;
import com.tottus.suull.entity.DetalleAlquiler;
import com.tottus.suull.entity.Movimiento;
import com.tottus.suull.entity.Precio;
import com.tottus.suull.entity.Proveedor;
import com.tottus.suull.entity.Stock;
import com.tottus.suull.entity.Tienda;
import com.tottus.suull.repository.RepoActivo;
import com.tottus.suull.repository.RepoAlquiler;
import com.tottus.suull.repository.RepoDetalleAlquiler;
import com.tottus.suull.repository.RepoMovimiento;
import com.tottus.suull.repository.RepoPrecio;
import com.tottus.suull.repository.RepoProveedor;
import com.tottus.suull.repository.RepoTienda;
import com.tottus.suull.service.SequenceGeneratorService;

@Controller
@RequestMapping("/alquiler")
public class AlquileresController {
	
	@Autowired
	private RepoAlquiler repoAlquiler;
	
	@Autowired
	private RepoPrecio repoPrecio;
	
	@Autowired
	private RepoActivo repoActivo;
	
	@Autowired
	private RepoMovimiento repoMovimiento;
	
	@Autowired
	private RepoTienda repoTienda;
	
	@Autowired
	private RepoProveedor repoProveedor;
	
	@Autowired
	private RepoDetalleAlquiler repoDetalleAlquiler;
	
    private SequenceGeneratorService sequenceGenerator;
	
	private String getNroOperacionEntrega() {
		return "E-"+repoAlquiler.findByOperacion("ENTREGA").size();
	}
	private String getNroOperacionDevolucion() {
		return "D-"+repoAlquiler.findByOperacion("DEVOLUCION").size();
	}

	@GetMapping("/entrega")
	public String operacionEntrega(Map<String, Object> model) {
		
		Tienda tienda = repoTienda.findByNumero(590).get();
		ArrayList<Activo> activos_alquiler = (ArrayList<Activo>) repoActivo.findByAlquiler(true);
		Alquiler alquiler = new Alquiler();
		ArrayList<DetalleAlquiler> detalle = new ArrayList<DetalleAlquiler>();
		for(int i=0;i<activos_alquiler.size();i++){
			DetalleAlquiler dto_alquiler = new DetalleAlquiler();
			
			dto_alquiler.setId(""+sequenceGenerator.generateSequence(DetalleAlquiler.SEQUENCE_NAME));
			dto_alquiler.setCantidad(0);
			dto_alquiler.setSku(activos_alquiler.get(i).getSku());
			dto_alquiler.setPrecio(activos_alquiler.get(i).getPrecio());
			dto_alquiler.setPrecio_no_lavado(activos_alquiler.get(i).getPrecio_no_lavado());
			dto_alquiler.setNombre(activos_alquiler.get(i).getNombre());
			detalle.add(dto_alquiler);
			activos_alquiler.get(i).setStock(tienda.getStockBySKU(activos_alquiler.get(i).getSku()));
		}
		alquiler.setDetalle(detalle);
		model.put("activos_alquiler", activos_alquiler);
		model.put("alquiler", alquiler);
		
		model.put("nro_operacion", getNroOperacionEntrega());
		
		model.put("menu", "alquileres");
		model.put("view_mode",false);
		model.put("mensaje","");
		model.put("titulo","Registrar Entrega");
		model.put("create", true);
		List<Proveedor> proveedores = this.repoProveedor.findAll();
		model.put("proveedores", proveedores);
		model.put("operacion_entrega",true);
		return "alquiler_entrega";
	}
	@GetMapping("/devolucion")
	public String operacionDevolucion(Map<String, Object> model) {
		
		
		Tienda tienda = repoTienda.findByNumero(590).get();
		ArrayList<Activo> activos_alquiler = (ArrayList<Activo>) repoActivo.findByAlquiler(true);
		Alquiler alquiler = new Alquiler();
		ArrayList<DetalleAlquiler> detalle = new ArrayList<DetalleAlquiler>();
		for(int i=0;i<activos_alquiler.size();i++){
			DetalleAlquiler dto_alquiler = new DetalleAlquiler();
			
			dto_alquiler.setId(""+sequenceGenerator.generateSequence(DetalleAlquiler.SEQUENCE_NAME));
			dto_alquiler.setCantidad(0);
			dto_alquiler.setSku(activos_alquiler.get(i).getSku());
			dto_alquiler.setPrecio(activos_alquiler.get(i).getPrecio());
			dto_alquiler.setNombre(activos_alquiler.get(i).getNombre());
			detalle.add(dto_alquiler);
			activos_alquiler.get(i).setStock(tienda.getStockBySKU(activos_alquiler.get(i).getSku()));
		}
		alquiler.setDetalle(detalle);
		model.put("activos_alquiler", activos_alquiler);
		model.put("alquiler", alquiler);
		model.put("nro_operacion", getNroOperacionDevolucion());
		model.put("menu", "alquileres");
		model.put("view_mode",false);
		model.put("mensaje","");
		model.put("titulo","Registar Devolución");
		model.put("create", true);
		List<Proveedor> proveedores = this.repoProveedor.findAll();
		model.put("proveedores", proveedores);
		model.put("operacion_entrega",true);
		return "alquiler_devolucion";
	}
	

	 @Autowired
	    public AlquileresController(SequenceGeneratorService sequenceGeneratorService) {
	        this.sequenceGenerator = sequenceGeneratorService;
	    }
	
	 
	 
	 private boolean activosATienda(ArrayList<DetalleAlquiler> activo_detalle,Tienda tienda_partida,Tienda tienda_entrega) {
			
			if(tienda_partida.getId().equals(tienda_entrega.getId())){
				return false;
			}
			System.out.print("\n---------ACTIVOS A TIENDA---------\n");
			System.out.print("---------DE "+tienda_partida.getNombre()+"---a---"+tienda_entrega.getNombre()+"----\n");
			for (int i = 0; i < activo_detalle.size(); i++) {
				DetalleAlquiler ad = activo_detalle.get(i);
				Integer stock_partida = tienda_partida.getStockBySKU(ad.getSku());
				Integer s1 = stock_partida - ad.getCantidad();
				System.out.print("---------s1 "+s1+"---------\n");
				if(s1>=0)tienda_partida.setStockBySKU(ad.getSku(), s1);
				Integer stock_entrega = tienda_entrega.getStockBySKU(ad.getSku());
				Integer s2 = stock_entrega + ad.getCantidad();
				System.out.print("---------s2 "+s2+"---------\n");
				if(s2>=0)tienda_entrega.setStockBySKU(ad.getSku(), s2);
			}
			this.repoTienda.save(tienda_partida);
			this.repoTienda.save(tienda_entrega);
			tienda_partida.updateActivos();
			tienda_entrega.updateActivos();
			this.repoTienda.save(tienda_partida);
			this.repoTienda.save(tienda_entrega);
			
			System.out.print("---------------------------\n");
			return true;
		}
	@PostMapping("/guardar")
	public String operacionSubmit(Map<String, Object> model, @Valid Alquiler alquiler, BindingResult result) {
		Precio precio = repoPrecio.findAll().get(0);
		model.put("precio", precio);
		String id = alquiler.getId();
		if (id.equals("")) {
			alquiler.setEstado("ACTIVO");
			model.put("titulo","Alquiler Creado");
			
			if(alquiler.getOperacion().equals("DEVOLUCION")) {
				model.put("mensaje", "Devolución creada con éxito");
			}
			if(alquiler.getOperacion().equals("ENTREGA")) {
				model.put("mensaje", "Entrega creada con éxito");
			}
			
		} else {
			model.put("titulo","Alquiler Actualizado");
			model.put("mensaje", "Alquiler actualizado con éxito");
		}
		alquiler.setUsuario(SecurityContextHolder.getContext().getAuthentication().getName());
		alquiler.setFecha(new Date());
		ArrayList<DetalleAlquiler> detalle = (ArrayList<DetalleAlquiler>) alquiler.getDetalle();
		Proveedor prov = alquiler.getProveedor();
		
		
		Double costo = 0.0;
		for(int i=0;i<detalle.size();i++) {
			DetalleAlquiler detalle_alquiler = detalle.get(i);
			if(detalle_alquiler.getCantidad()>0) {
				if(alquiler.getOperacion().equals("ENTREGA")) {
					if(detalle_alquiler.isNo_lavado()) {
						costo += detalle_alquiler.getCantidad()*detalle.get(i).getPrecio_no_lavado();
					}else {
						costo += detalle_alquiler.getCantidad()*detalle.get(i).getPrecio();
					}
				}
				this.repoDetalleAlquiler.insert(detalle_alquiler);
			}
		}
		if(alquiler.getOperacion().equals("ENTREGA")) {
			alquiler.setCosto(costo);
		}else {
			alquiler.setCosto(0.0);
		}
		alquiler.setProveedor_nombre(alquiler.getProveedor().getRazon_social());
		String _id_alquiler  = ""+sequenceGenerator.generateSequence(Alquiler.SEQUENCE_NAME);
		
		System.out.print("_id_alquiler "+_id_alquiler);
		alquiler.setId(_id_alquiler);
		repoAlquiler.insert(alquiler);
		Alquiler nuevo_alquiler = repoAlquiler.findById(_id_alquiler).get();
		
		if(alquiler.getOperacion().equals("ENTREGA")) {
			alquiler.setTotal(updateProveedorByDetalle(prov,detalle,"ENTREGA",nuevo_alquiler));
		}else {
			alquiler.setTotal(updateProveedorByDetalle(prov,detalle,"DEVOLUCION",nuevo_alquiler));
		}
		Tienda tienda_partida;
		Tienda tienda_entrega;
		if(alquiler.getOperacion().equals("ENTREGA")) {
			tienda_partida = this.repoTienda.findByNumero(590).get();
			tienda_entrega = this.repoTienda.findByNombre("Alquiler");
		}else {
			tienda_partida = this.repoTienda.findByNombre("Alquiler");
			tienda_entrega = this.repoTienda.findByNumero(590).get();
		}
		activosATienda(detalle,tienda_partida,tienda_entrega);
		model.put("nro_operacion", alquiler.getNro_operacion());
		model.put("menu", "alquileres");
		model.put("view_mode",false);
		model.put("create", false);
		
		if(alquiler.getOperacion().equals("DEVOLUCION")) {
			return "alquiler_devolucion";
		}else{
			return "alquiler_entrega";
		}
		
	}
	private Integer updateProveedorByDetalle(Proveedor prov,ArrayList<DetalleAlquiler> detalle,String tipo,Alquiler nuevo_alquiler) {
		ArrayList<ActivoDetalleProveedor> proveedor_detalle = prov.getDetalle();
		ArrayList<ActivoDetalleProveedor> new_proveedor_detalle = new ArrayList<ActivoDetalleProveedor>();
		Integer total = 0;
		for(int i=0;i<detalle.size();i++) {
			boolean exist = false;
			Integer cantidad_previa = 0;
			if(proveedor_detalle!=null) {
				for(int p=0;p<proveedor_detalle.size();p++) {
					if(detalle.get(i).getSku().equals(proveedor_detalle.get(p).getSku())) {
						cantidad_previa = proveedor_detalle.get(p).getCantidad();
						exist = true;
					}
				}
			}
			Integer cantidad_actual = 0;
			Integer factor = 1;
			if(tipo.equals("ENTREGA")) {
				cantidad_actual = cantidad_previa + detalle.get(i).getCantidad();
			}else {
				factor=-1;
				cantidad_actual = cantidad_previa - detalle.get(i).getCantidad();
			}
			if(cantidad_actual<0)cantidad_actual=0;
			
			if(nuevo_alquiler!=null){
				
				
				if(tipo.equals("ENTREGA")) {
					
					if(detalle.get(i).getCantidad()>0) {
						createMov(detalle.get(i),true,cantidad_actual,detalle.get(i).getCantidad()*factor,prov,nuevo_alquiler);
					}
					
				}else {
					Activo activo = this.repoActivo.findBySku(detalle.get(i).getSku());
					ArrayList<Movimiento> movs = (ArrayList<Movimiento>) repoMovimiento.findByProveedorAndActivo(prov.getId(), activo.getId());
					Integer cantidad_devolver = detalle.get(i).getCantidad();
					for(int k=0;k<movs.size();k++) {
						Movimiento m = movs.get(k);
						if(m.getCantidad()!=m.getDevuelto()) {
							if(cantidad_devolver!=0) {
								Movimiento _m = new Movimiento();
								_m.setId(sequenceGenerator.generateSequence(Movimiento.SEQUENCE_NAME)+"");
								Integer devuelto = m.getDevuelto();
								Integer espacio = m.getCantidad()-m.getDevuelto();
								if(cantidad_devolver<=espacio) {
									m.setDevuelto(devuelto+cantidad_devolver);
									_m.setCantidad(cantidad_devolver*-1);
									cantidad_devolver -= cantidad_devolver;
								}else {
									m.setDevuelto(devuelto+espacio);
									_m.setCantidad(espacio*-1);
									cantidad_devolver -= espacio;
								}
								repoMovimiento.save(m);
								_m.setPrecio(m.getPrecio());
								_m.setUsuario(SecurityContextHolder.getContext().getAuthentication().getName());
								_m.setActivo(m.getActivo());
								_m.setFecha(nuevo_alquiler.getFecha_devolucion());
								_m.setAlquiler(nuevo_alquiler);
								_m.setProveedor(prov);
								repoMovimiento.insert(_m);
							}

						}
					}
				}
				
			}

			ActivoDetalleProveedor _detalle_prov = new ActivoDetalleProveedor();
			_detalle_prov.setSku(detalle.get(i).getSku());
			_detalle_prov.setCantidad(cantidad_actual);
			new_proveedor_detalle.add(_detalle_prov);
			total+=detalle.get(i).getCantidad();
		}
		prov.setDetalle(new_proveedor_detalle);
		this.repoProveedor.save(prov);
		return total;
	}
	
	private void createMov(DetalleAlquiler detalle,boolean lavada,Integer stock,Integer cantidad,Proveedor prov,Alquiler alquiler) {
		Activo ac = this.repoActivo.findBySku(detalle.getSku());
		Movimiento mov = new Movimiento();
		mov.setId(sequenceGenerator.generateSequence(Movimiento.SEQUENCE_NAME)+"");
		mov.setActivo(ac);
		mov.setStock(stock);
		mov.setDevuelto(0);
		if(detalle.isNo_lavado()) {
			mov.setPrecio(detalle.getPrecio_no_lavado());
		}else {
			mov.setPrecio(detalle.getPrecio());
		}
		mov.setUsuario(SecurityContextHolder.getContext().getAuthentication().getName());
		mov.setCantidad(cantidad);
		mov.setProveedor(prov);
		mov.setAlquiler(alquiler);
		if(alquiler.getOperacion().equals("DEVOLUCION")) {
			mov.setFecha(alquiler.getFecha_devolucion());
		}else {
			mov.setFecha(alquiler.getFecha_alquiler());
		}
		
		this.repoMovimiento.save(mov);
	}
	@GetMapping("/ver/{id}")
	public String operacionVer(Model model, @PathVariable("id") String id) {
		
		
		Alquiler t = repoAlquiler.findById(id).get();
		
		Precio precio = repoPrecio.findAll().get(0);
		model.addAttribute("precio", precio);
		
		List<Proveedor> clientes = this.repoProveedor.findAll();
		
		if(t.getOperacion().equals("ENTREGA")) {
			model.addAttribute("operacion_entrega",true);
		}else {
			model.addAttribute("operacion_entrega",false);
		}
		
		model.addAttribute("proveedor_seleccionado",t.getProveedor().getId());
				
		model.addAttribute("nro_operacion",t.getNro_operacion());
		model.addAttribute("estado",t.getEstado());
		model.addAttribute("operacion",t.getOperacion());
		//model.addAttribute("cliente_seleccionado",t.getProveedor().getId());
		model.addAttribute("proveedores", clientes);
		model.addAttribute("view_mode",true);
		model.addAttribute("menu", "alquileres");
		model.addAttribute("create", true);
		model.addAttribute("alquiler", t);
		model.addAttribute("titulo","Alquiler");
		if(t.getOperacion().equals("ENTREGA")) {
			return "alquiler_entrega";
		}else {
			return "alquiler_devolucion";
		}
		
	}
	@GetMapping("/editar/{id}")
	public String operacionEditar(Model model, @PathVariable("id") String id) {
		Alquiler t = repoAlquiler.findById(id).get();
		
		Precio precio = repoPrecio.findAll().get(0);
		model.addAttribute("precio", precio);
		
		List<Proveedor> clientes = this.repoProveedor.findAll();
		
		model.addAttribute("operacion",t.getOperacion());
		model.addAttribute("cliente_seleccionado",t.getProveedor().getId());
		model.addAttribute("clientes", clientes);
		model.addAttribute("view_mode",false);
		model.addAttribute("menu", "alquileres");
		model.addAttribute("create", true);
		model.addAttribute("alquiler", t);
		model.addAttribute("titulo","Alquiler");
		return "alquiler";
	}
	
	@GetMapping("/eliminar/{id}")
	public String alquilerEliminar(Model model, @PathVariable("id") String id) {
		model.addAttribute("menu", "alquileres");
		repoAlquiler.deleteById(id);
		return "redirect:/alquiler/listar";
	}
	
	@GetMapping("/anular/{id}")
	public String operacionCancelar(Model model, @PathVariable("id") String id) {
		
		Alquiler alquiler = repoAlquiler.findById(id).get();
		alquiler.setEstado("ANULADO");
		
		List<Movimiento> movimientos = this.repoMovimiento.findByAlquiler(alquiler.getId());
		for(int p=0;p<movimientos.size();p++) {
			Movimiento mov = movimientos.get(p);
			this.repoMovimiento.delete(mov);
		}
		Proveedor prov = alquiler.getProveedor();
		if(alquiler.getOperacion().equals("ENTREGA")) {
			updateProveedorByDetalle(prov,(ArrayList<DetalleAlquiler>)alquiler.getDetalle(),"DEVOLUCIOM",null);
		}else {
			updateProveedorByDetalle(prov,(ArrayList<DetalleAlquiler>)alquiler.getDetalle(),"ENTREGA",null);
		}
		Tienda tienda_partida = this.repoTienda.findByNombre("Alquiler");
		Tienda tienda_entrega = this.repoTienda.findByNumero(590).get();
		activosATienda((ArrayList<DetalleAlquiler>)alquiler.getDetalle(),tienda_partida,tienda_entrega);
		
		
		repoAlquiler.save(alquiler);
		model.addAttribute("menu", "alquileres");
		return "redirect:/alquiler/listar";
	}
	
	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public String listAlquileres(Model model) {
		model.addAttribute("menu", "alquileres");
		
		
		Integer todos = repoAlquiler.findByEstado("ACTIVO").size();
		Integer entregas = repoAlquiler.findByEstadoAndOperacion("ACTIVO","ENTREGA").size();
		Integer devolucion = repoAlquiler.findByEstadoAndOperacion("ACTIVO","DEVOLUCION").size();
		Integer anuladas = repoAlquiler.findByEstado("ANULADO").size();
		
		model.addAttribute("activos", getActivosAlquiler());
		model.addAttribute("todos", todos);
		model.addAttribute("entregas", entregas);
		model.addAttribute("devoluciones", devolucion);
		model.addAttribute("anulados", anuladas);
		
		
		model.addAttribute("tipo", "todos");

		return "alquileres";
	}
	private ArrayList<Activo> getActivosAlquiler(){
		ArrayList<Activo> activos_alquiler = (ArrayList<Activo>) repoActivo.findByAlquiler(true);
		/*for(int i=0;i<activos_alquiler.size();i++){
			activos_alquiler.get(i).setPos(i);
		}*/
		return activos_alquiler;
	}
	
	@RequestMapping(value = "/listar/{tipo}", method = RequestMethod.GET)
	public String listAlquilerTipo(Model model,@PathVariable("tipo") String tipo) {
		
		Integer todos = repoAlquiler.findByEstado("ACTIVO").size();
		Integer entregas = repoAlquiler.findByEstadoAndOperacion("ACTIVO","ENTREGA").size();
		Integer devolucion = repoAlquiler.findByEstadoAndOperacion("ACTIVO","DEVOLUCION").size();
		Integer anuladas = repoAlquiler.findByEstado("ANULADO").size();
		
		model.addAttribute("activos", getActivosAlquiler());
		model.addAttribute("todos", todos);
		model.addAttribute("entregas", entregas);
		model.addAttribute("devoluciones", devolucion);
		model.addAttribute("anulados", anuladas);
		
		model.addAttribute("tipo",tipo);
		return "alquileres";
	}
	
	
	
	@GetMapping("/precios")
	public String preciosForm(Map<String, Object> model) {
		
		Precio precio = repoPrecio.findAll().get(0);
		
		model.put("precio", precio);
		model.put("menu", "precios");
		model.put("view_mode",false);
		model.put("mensaje","");
		model.put("create", true);
				
		return "precios";
	}
	@PostMapping("/precios")
	public String preciosSubmit(Map<String, Object> model, @Valid Precio precio, BindingResult result) {
		
		String id = precio.getId();
		if (!id.equals("")) {
			Precio p = repoPrecio.findById(id).get();
			p.setTipo_1(precio.getTipo_1());
			p.setTipo_2(precio.getTipo_2());
			p.setTipo_3(precio.getTipo_3());
			p.setTipo_4(precio.getTipo_4());
			repoPrecio.save(p);
			model.put("precio", p);
			model.put("mensaje", "Precios actualizados con éxito");
		}
		
		return "precios";
	}
	/*private String getCurrentUsername() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getName();
		return user.getUsername();
	}*/

}
