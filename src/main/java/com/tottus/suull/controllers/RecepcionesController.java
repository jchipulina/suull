
package com.tottus.suull.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tottus.suull.entity.Activo;
import com.tottus.suull.entity.ActivoDetalle;
import com.tottus.suull.entity.Devolucion;
import com.tottus.suull.entity.Operacion;
import com.tottus.suull.entity.Tienda;
import com.tottus.suull.entity.Transporte;
import com.tottus.suull.repository.RepoActivo;
import com.tottus.suull.repository.RepoDevolucion;
import com.tottus.suull.repository.RepoOperacion;
import com.tottus.suull.repository.RepoTienda;
import com.tottus.suull.repository.RepoTransporte;
import com.tottus.suull.repository.RepoUsers;


@Controller
@RequestMapping("/recepcion")
public class RecepcionesController {
	
	@Autowired
	private RepoOperacion repoOperacion;
	
	@Autowired
	private RepoDevolucion repoDevolucion;
	
	@Autowired
	private RepoTransporte repoTransporte;
	
	@Autowired
	private RepoActivo repoActivo;
	
	@Autowired
	private RepoTienda repoTienda;
	
	@Autowired
	private RepoUsers repoUsers;
	

	 
	@GetMapping("/ver/{id}")
	public String operacionVer(Model model, @PathVariable("id") String id) {
		
		
		List<Tienda> tiendas = this.repoTienda.findAll();
		model.addAttribute("tiendas", tiendas);
		List<Transporte> transportes = this.repoTransporte.findAll();
		model.addAttribute("transportes", transportes);
		List<Activo> activos = this.repoActivo.findAll();
		model.addAttribute("activos", activos);
		
		if(getRole().equals("ROLE_SUULL_TIENDA")) {
			Operacion t = repoOperacion.findById(id).get();
			Integer total = 0;
			for(int i=0;i<t.getActivos().size();i++) {
				total += t.getActivos().get(i).getCantidad();
			}
			model.addAttribute("observacion_tienda",t.getObservacion_tienda());
			model.addAttribute("total_activos", total);
			model.addAttribute("nro_operacion", t.getNro_operacion());
			model.addAttribute("despacho", t);
			model.addAttribute("estado",t.getEstado());
			
			model.addAttribute("tipo_operacion","operacion");
			
			model.addAttribute("tienda_origen",t.getLocal_partida().getId());
			model.addAttribute("tienda_destino",t.getLocal_entrega().getId());
			
		}else {
			System.out.print("id repoDevolucion "+id);
			Devolucion t = repoDevolucion.findById(id).get();
			Integer total = 0;
			for(int i=0;i<t.getActivos().size();i++) {
				total += t.getActivos().get(i).getCantidad();
			}
			
			model.addAttribute("tienda_origen",t.getLocal_partida().getId());
			model.addAttribute("tienda_destino",t.getLocal_entrega().getId());
			
			model.addAttribute("observacion_tienda",t.getObservacion_tienda());
			model.addAttribute("total_activos", total);
			model.addAttribute("nro_operacion", t.getNro_operacion());
			model.addAttribute("despacho", t);
			model.addAttribute("estado",t.getEstado());
			
			model.addAttribute("tipo_operacion","devolucion");
		}
		
		
		
		
		model.addAttribute("view_mode",true);
		
		model.addAttribute("menu", "recepcion");
		model.addAttribute("create", true);
		
		
		model.addAttribute("tipo","recepcion");
		
		return "recepcion";
	}
	@PostMapping("/recepcionar")
	public String operacionRecepcionar(Map<String, Object> model, @Valid Operacion operacion, BindingResult result) {
		
		
		String id = operacion.getId();
		Operacion t = repoOperacion.findById(id).get();
		t.setEstado("RECEPCIONADO");
		t.setFecha_recepcion(new Date());
		
		repoOperacion.save(t);
		model.put("menu", "recepcion");
		return "redirect:/recepcion/listar/3";
	}

	
	@RequestMapping(value = "recepcionar", params = {"operacion_observar"})
	public String operacionObservar(Map<String, Object> model, @Valid Operacion operacion, BindingResult result) {
		String id = operacion.getId();
		Operacion t = repoOperacion.findById(id).get();
		t.setEstado("OBSERVADO");
		System.out.print("\n---------ANT ACTIVOS A TIENDA-1--------\n");
		System.out.print("\n---------===========2--------\n"+t.toString());
		activosATienda(t.getActivos(),this.getCurrentTienda(),t.getLocal_partida());
		t.setObservacion_tienda(operacion.getObservacion_tienda());
		repoOperacion.save(t);
		model.put("menu", "recepcion");
		return "redirect:/recepcion/listar/2";
	}
	
	
	
	@RequestMapping(value = "recepcionar", params = {"devolucion"})
	public String operacionRecepcionarDevolucion(Map<String, Object> model, @Valid Devolucion operacion, BindingResult result) {
		String id = operacion.getId();
		Devolucion t = repoDevolucion.findById(id).get();
		t.setEstado("RECEPCIONADO");
		t.setFecha_recepcion(new Date());
		
		boolean tranferencia_pura = t.getTransferencia_pura();
		
		ArrayList<ActivoDetalle> activo_detalle = t.getActivos();
		Tienda tienda_partida = t.getLocal_partida();
		Tienda tienda_entrega = t.getLocal_entrega();
		for (int i = 0; i < activo_detalle.size(); i++) {
			ActivoDetalle ad = activo_detalle.get(i);
			
			if(!tranferencia_pura) {
				Integer stock_partida = tienda_partida.getStockBySKU(ad.getSku());
				Integer s1 = stock_partida - ad.getCantidad();
				if(s1>=0)tienda_partida.setStockBySKU(ad.getSku(),s1);
			}
			Integer stock_entrega = tienda_entrega.getStockBySKU(ad.getSku());
			Integer s2 = stock_entrega + ad.getCantidad();
			if(s2>=0)tienda_entrega.setStockBySKU(ad.getSku(), s2);
		}
		this.repoTienda.save(tienda_partida);
		this.repoTienda.save(tienda_entrega);
		tienda_partida.updateActivos();
		tienda_entrega.updateActivos();
		this.repoTienda.save(tienda_partida);
		this.repoTienda.save(tienda_entrega);
		
		repoDevolucion.save(t);
		model.put("menu", "recepcion");
		return "redirect:/recepcion/listar/3";
	}
	@RequestMapping(value = "recepcionar", params = {"devolucion_observar"})
	public String operacionObservarDevolucion(Map<String, Object> model, @Valid Devolucion operacion, BindingResult result) {
		String id = operacion.getId();
		Devolucion t = repoDevolucion.findById(id).get();
		t.setEstado("OBSERVADO");
		System.out.print("\n---------ANT ACTIVOS A TIENDA-2--------\n");
		System.out.print("\n---------===========1--------\n"+t.toString());
		//activosATienda(t.getActivos(),this.getCurrentTienda(),t.getLocal_partida());
		
		t.setObservacion_tienda(operacion.getObservacion_tienda());
		repoDevolucion.save(t);
		model.put("menu", "recepcion");
		return "redirect:/recepcion/listar/2";
	}
	private boolean activosATienda(ArrayList<ActivoDetalle> activo_detalle,Tienda tienda_partida,Tienda tienda_entrega) {
		
		System.out.print("\n---------ACTIVOS A TIENDA---------\n");
		System.out.print("\n---------*****1--------\n");
		System.out.print("\n---------tienda_partida "+tienda_partida.toString()+"---------\n");
		System.out.print("\n---------*****2--------\n");
		System.out.print("\n---------tienda_entrega "+tienda_entrega.toString()+"---------\n");
		
		if(tienda_partida.getId().equals(tienda_entrega.getId())){
			return false;
		}
		
		System.out.print("---------DE "+tienda_partida.getNombre()+"---a---"+tienda_entrega.getNombre()+"----\n");
		for (int i = 0; i < activo_detalle.size(); i++) {
			ActivoDetalle ad = activo_detalle.get(i);
			Integer stock_partida = tienda_partida.getStockBySKU(ad.getSku());
			Integer s1 = stock_partida - ad.getCantidad();
			System.out.print("---------s1 "+s1+"---------\n");
			if(s1>=0)tienda_partida.setStockBySKU(ad.getSku(), s1);
			Integer stock_entrega = tienda_entrega.getStockBySKU(ad.getSku());
			Integer s2 = stock_entrega + ad.getCantidad();
			System.out.print("---------s2 "+s2+"---------\n");
			if(s2>=0)tienda_entrega.setStockBySKU(ad.getSku(), s2);
		}
		this.repoTienda.save(tienda_partida);
		this.repoTienda.save(tienda_entrega);
		tienda_partida.updateActivos();
		tienda_entrega.updateActivos();
		this.repoTienda.save(tienda_partida);
		this.repoTienda.save(tienda_entrega);
		
		System.out.print("---------------------------\n");
		return true;
	}
	private Model getInfoOperacion(Model model,Integer tipo){
		
		
		Tienda tienda = getCurrentTienda();
		Integer cerradas = 0;
		Integer observadas = 0;
		Integer recepcionadas = 0;
		if(getRole().equals("ROLE_SUULL_TIENDA")) {
			cerradas = repoOperacion.findByEstadoAndLocalentregaid("CERRADO",tienda.getId()).size();
			observadas = repoOperacion.findByEstadoAndLocalentregaid("OBSERVADO",tienda.getId()).size();
			recepcionadas = repoOperacion.findByEstadoAndLocalentregaid("RECEPCIONADO",tienda.getId()).size();
		}else {
			cerradas = repoDevolucion.findByEstadoAndLocalentregaid("CERRADO",tienda.getId()).size();
			observadas = repoDevolucion.findByEstadoAndLocalentregaid("OBSERVADO",tienda.getId()).size();
			recepcionadas = repoDevolucion.findByEstadoAndLocalentregaid("RECEPCIONADO",tienda.getId()).size();
		}
		

		Integer total = cerradas + observadas + recepcionadas;
		model.addAttribute("todas",total);
		model.addAttribute("cerradas",cerradas);
		model.addAttribute("observadas",observadas);
		model.addAttribute("recepcionadas",recepcionadas);
		
		model.addAttribute("menu", "recepcion");
		if(tipo==-1) {
			model.addAttribute("estado","TODOS");
		}
		if(tipo==0) {
			model.addAttribute("estado","ABIERTO");
		}
		if(tipo==1) {
			model.addAttribute("estado","CERRADO");
		}
		if(tipo==2) {
			model.addAttribute("estado","OBSERVADO");
		}
		if(tipo==3) {
			model.addAttribute("estado","RECEPCIONADO");
		}
		model.addAttribute("tipo","recepcion");
		
		return model;
	}
	String getRole() {
		String s_auth  = SecurityContextHolder.getContext().getAuthentication().getAuthorities().iterator().next().getAuthority();
		return s_auth;
	}
	@RequestMapping(value = "/listar", method = RequestMethod.GET)
	public String listOperaciones(Model model) {
		model = getInfoOperacion(model,-1);
		if(getRole().equals("ROLE_SUULL_TIENDA")) {
			return "despachos";
		}else {
			return "devoluciones";
		}
		
	}
	
	@RequestMapping(value = "/listar/{tipo}", method = RequestMethod.GET)
	public String listOperacionesTipo(Model model,@PathVariable("tipo") Integer tipo) {
		model = getInfoOperacion(model,tipo);
		if(getRole().equals("ROLE_SUULL_TIENDA")) {
			return "despachos";
		}else {
			return "devoluciones";
		}
	}
	private Tienda getCurrentTienda() {
		DefaultOidcUser user = (DefaultOidcUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return repoUsers.findById(user.getAttribute("oid")).get().getTienda();
	}
}
