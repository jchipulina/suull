/**
 * 
 */
package com.tottus.suull.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.tottus.suull.entity.Activo;
import com.tottus.suull.entity.ActivoDetalle;
import com.tottus.suull.entity.ActivoDetalleProveedor;
import com.tottus.suull.entity.ActivoRef;
import com.tottus.suull.entity.Compra;
import com.tottus.suull.entity.Proveedor;
import com.tottus.suull.entity.ProveedorActivo;
import com.tottus.suull.entity.ReporteProveedor;
import com.tottus.suull.entity.Role;
import com.tottus.suull.entity.Stock;
import com.tottus.suull.entity.Stocksave;
import com.tottus.suull.entity.Tienda;
import com.tottus.suull.entity.Transporte;
import com.tottus.suull.entity.Users;
import com.tottus.suull.repository.RepoActivo;
import com.tottus.suull.repository.RepoCliente;
import com.tottus.suull.repository.RepoCompra;
import com.tottus.suull.repository.RepoDevolucion;
import com.tottus.suull.repository.RepoOperacion;
import com.tottus.suull.repository.RepoProveedor;
import com.tottus.suull.repository.RepoProveedorActivo;
import com.tottus.suull.repository.RepoReporteProveedor;
import com.tottus.suull.repository.RepoRole;
import com.tottus.suull.repository.RepoTienda;
import com.tottus.suull.repository.RepoTransporte;
import com.tottus.suull.repository.RepoUsers;
import com.tottus.suull.service.SequenceGeneratorService;


/**
 * @author pavan.solapure
 *
 */

class Option {
	public String label;
}

@ConditionalOnProperty(prefix = "azure.activedirectory", value = "tenant-id")
@Controller
public class BaseController {

	@Autowired
	private RepoActivo repoActivo;
	@Autowired
	private RepoCliente repoCliente;
	@Autowired
	private RepoProveedor repoProveedor;
	
	@Autowired
	private RepoProveedorActivo repoProveedorActivo;
	
	@Autowired
	private RepoOperacion repoOperacion;
	@Autowired
	private RepoProveedor repoProveedores;
	
	@Autowired
	private RepoProveedorActivo repoProveedoresActivos;
	
	
	@Autowired
	private RepoDevolucion repoDevolucion;
	
	@Autowired private RepoReporteProveedor repoReporteProveedor;
	
	@Autowired
	private RepoCompra repoCompra;
	@Autowired
	private RepoTransporte repoTransporte;
	@Autowired
	private RepoUsers repoUsers;
	@Autowired
	private RepoRole repoRole;
	@Autowired
	private RepoTienda repoTienda;
	// @Autowired private RepoAlquiler repoAlquiler;

	/** The entity manager. */
	@PersistenceContext
	private EntityManager entityManager;
	
	//@Autowired
    //private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	 @Autowired
	 private OAuth2AuthorizedClientService authorizedClientService;
	 
	private SequenceGeneratorService sequenceGenerator;
	
	 @Autowired
	    public BaseController(SequenceGeneratorService sequenceGeneratorService) {
	        this.sequenceGenerator = sequenceGeneratorService;
	    }
	

	Users getByAuth(OAuth2AuthenticationToken authentication) {
		final OAuth2AuthorizedClient authorizedClient =
	             this.authorizedClientService.loadAuthorizedClient(
	                     authentication.getAuthorizedClientRegistrationId(),
	                     authentication.getName());
	    
	     //System.out.print(" authentication.getName() "+authentication.getName());
	     //System.out.print(" authentication.getAuthorizedClientRegistrationId() "+authentication.getAuthorizedClientRegistrationId());
	     if(authorizedClient!=null) {
	    	 //System.out.print("\n authorizedClient !! "+authorizedClient.getClientRegistration().getClientId());
	    	 
		    
		     
		     
		     //System.out.print("clientId "+clientId);
		     System.out.print("authentication.getName() "+authentication.getName());
		     
		    
		     
		     DefaultOidcUser user = (DefaultOidcUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		     
		     String clientId = user.getAttribute("oid");
		     Optional<Users> u = repoUsers.findById(clientId);
		    
		     
				/*
				 * System.out.print("---------------------");
				 * System.out.print("---------------------");
				 * System.out.print("---------------------");
				 * System.out.print(user.getFamilyName());
				 * System.out.print("---------------------"); System.out.print(clientId);
				 * 
				 * System.out.print("---------------------");
				 * System.out.print("---------------------");
				 * System.out.print("---------------------");
				 * 
				 * //String accessToken =
				 * "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6Imh1Tjk1SXZQZmVocTM0R3pCRFoxR1hHaXJuTSIsImtpZCI6Imh1Tjk1SXZQZmVocTM0R3pCRFoxR1hHaXJuTSJ9.eyJhdWQiOiJodHRwczovL2dyYXBoLndpbmRvd3MubmV0IiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvZWU3ZWU2YWQtYWJiMi00YzA4LWFhMmUtZDVlOTg4NjMwMWQ5LyIsImlhdCI6MTU5NzI4MDA2MSwibmJmIjoxNTk3MjgwMDYxLCJleHAiOjE1OTcyODM5NjEsImFjciI6IjEiLCJhaW8iOiJBU1FBMi84UUFBQUE1RVlUVEpXcC9VSzJONW8yUE1OVHorRmF0dHN0QnpXSXg4VjhUdFRnaXc0PSIsImFtciI6WyJwd2QiXSwiYXBwaWQiOiJkM2NlNGNmOC02ODEwLTQ0MmQtYjQyZS0zNzVlMTQ3MTAwOTUiLCJhcHBpZGFjciI6IjAiLCJmYW1pbHlfbmFtZSI6Ikxlb24iLCJnaXZlbl9uYW1lIjoiUGVkcm8iLCJpcGFkZHIiOiIxOTAuMjM3LjE4MS4xOTIiLCJuYW1lIjoiUGVkcm8gTGVvbiIsIm9pZCI6ImUwYWFmM2U5LTE4ODEtNDJkZi04ZTExLTdhNjk1NGFlZTllMSIsInB1aWQiOiIxMDAzMjAwMEQ2NzM3RDEwIiwic2NwIjoiRGlyZWN0b3J5LkFjY2Vzc0FzVXNlci5BbGwgVXNlci5SZWFkIiwic3ViIjoib2ZrVmRIUTZLT0VMdm12WjhFdWJteGVjX19fN3NjQnNMd1lpVFVGSFJKdyIsInRlbmFudF9yZWdpb25fc2NvcGUiOiJTQSIsInRpZCI6ImVlN2VlNmFkLWFiYjItNGMwOC1hYTJlLWQ1ZTk4ODYzMDFkOSIsInVuaXF1ZV9uYW1lIjoicGxlb25AaW50ZXJhY3RpdmU2NjUub25taWNyb3NvZnQuY29tIiwidXBuIjoicGxlb25AaW50ZXJhY3RpdmU2NjUub25taWNyb3NvZnQuY29tIiwidXRpIjoiZUhyVW96RVBhay1wSVN2clhROEZBQSIsInZlciI6IjEuMCJ9.iQo5DG_TZ0HLU76PJvdRIKd-88Ojp2iuVoovpObtu1Qq4Hz1eUpLOom9IFvF1mlMLP8RmNVPLXFvyNTl9SYHWZGICSXvR1r05PYFqrY7hKyezsYVfmVX1LKT-4Q2gJsRz6aBa78ToynbcDI6ONyxPX_6XirCW5NWws7pHULf072podk_liyxzMu-7yxYNOcuoBvpoHRdnCviyl1_3Ey56FQyPuHzewfCQs9d3BQ51iAXVO8jnvZTx3XakNnpMlPgRK2CyRSIZYDbFdCDh90CdBEOepKhdOSDBDsiIuHtjnyGSsZYsdW3UMtMdBzm_LYRr-vZ4TemVAluLmLjiR2xsg";
				 * 
				 * DefaultOidcUser _user = (DefaultOidcUser)authentication.getPrincipal();
				 * String accessToken = _user.getIdToken().getTokenValue();
				 * 
				 * System.out.print("\n--------accessToken---"+accessToken);
				 * 
				 * System.out.print("\n");
				 * 
				 * 
				 * String yourtenantId = "ee7ee6ad-abb2-4c08-aa2e-d5e9886301d9"; // String url =
				 * "https://graph.windows.net/"+yourtenantId+"/users/"+clientId+
				 * "?api-version=1.6"; //take Azure AD graph for example.
				 * 
				 * String url =
				 * "https://graph.windows.net/ee7ee6ad-abb2-4c08-aa2e-d5e9886301d9/users/e0aaf3e9-1881-42df-8e11-7a6954aee9e1?api-version=1.5";
				 * HttpClient client = HttpClientBuilder.create().build(); HttpGet request = new
				 * HttpGet(url);
				 * 
				 * request.addHeader("Authorization","Bearer "+ accessToken);
				 * 
				 * HttpResponse response; try { response = client.execute(request); HttpEntity
				 * entity = response.getEntity(); String content = EntityUtils.toString(entity);
				 * JsonObject jsonObject = (JsonObject) new JsonParser().parse(content);
				 * 
				 * System.out.print(" content "+content);
				 * 
				 * } catch (ClientProtocolException e1) { // TODO Auto-generated catch block
				 * e1.printStackTrace(); } catch (IOException e1) { // TODO Auto-generated catch
				 * block e1.printStackTrace(); }
				 */

		     if(!u.isPresent()) {
		    	 Users _u = new Users();
		    	 _u.setId(clientId);
		    	 _u.setNombres(authentication.getName());
		    	 String rol = getRole();
		    	 _u.setEmail(user.getAttribute("unique_name"));
		    	 
		    	 if(rol.equals("ROLE_SUULL_TIENDA")) {
		    		 Integer tiendaNro = Integer.parseInt(user.getFamilyName());
		    		 Optional<Tienda> _tienda = repoTienda.findByNumero(tiendaNro);
		    		 if(_tienda.isPresent()) {
		    			 _u.setTienda(_tienda.get());
		    		 }
		    	 }
		    	 if(rol.equals("ROLE_SUULL_TIENDACENTRAL")) {
		    		 _u.setTienda(repoTienda.findByNumero(590).get());
		    	 }
		    	 if(rol.equals("ROLE_USER")) {
		    		 _u.setTienda(repoTienda.findByNumero(590).get());
		    	 }
		    	 if(rol.equals("ROLE_SUULL_ALQUILERES")) {
		    		 _u.setTienda(repoTienda.findByNumero(1000).get());
		    	 }
		    	 _u.setRol(getRole());
		    	 _u.setFecha_creacion(new Date());
		    	 repoUsers.insert(_u);
		    	 return _u;
		     }else {
		    	 return u.get();
		     }
	     }
	     return null;
	} 
	@GetMapping("/")
	public String index(Model model, OAuth2AuthenticationToken authentication) {
		
		
		Users u = getByAuth(authentication);
	     
		 //model.addAttribute("userName", u.getNombres());
	     //model.addAttribute("clientName","- ??????????");
	      
	     model.addAttribute("nombre_completo",u.getNombres()); 
	     
		if(u.getTienda()==null) {
			 model.addAttribute("tienda_no_especificada",true); 
			 model.addAttribute("tienda_direccion","TIENDA NO ESPECIFICADA");
			return "index";
		}
		model.addAttribute("tienda_no_especificada",false); 
		
		
		
		Tienda tienda_actual =  u.getTienda();
		String currentStoreId = tienda_actual.getId();
	
		if(tienda_actual.getNumero()==1000) {
			 model.addAttribute("tienda_desc","Alquileres");
			 model.addAttribute("tienda_direccion","");		 
		}else {
			 model.addAttribute("tienda_desc",tienda_actual.getNombre()+" ("+tienda_actual.getNumero()+")");
			 model.addAttribute("tienda_direccion",tienda_actual.getDireccion()+" "+u.getRol());
		}
	   
	     

		
		Integer abiertas = 0;
		Integer cerradas = 0;
		Integer observadas = 0;
		Integer recepcionadas = 0;
		//System.out.print("u.getTienda().getId() "+u.getTienda().getId());
		Integer r1 = 0;
		Integer r2 = 0;
		Integer r3 = 0;
		if(getRole().equals("ROLE_SUULL_TIENDA")) {
			
			abiertas = repoDevolucion.findByEstadoAndLocalpartidaid("ABIERTO",currentStoreId).size();
			cerradas = repoDevolucion.findByEstadoAndLocalpartidaid("CERRADO",currentStoreId).size();
			observadas = repoDevolucion.findByEstadoAndLocalpartidaid("OBSERVADO",currentStoreId).size();
			recepcionadas = repoDevolucion.findByEstadoAndLocalpartidaid("RECEPCIONADO",currentStoreId).size();
			
			r1 = repoOperacion.findByEstadoAndLocalentregaid("CERRADO",currentStoreId).size();
			r2 = repoOperacion.findByEstadoAndLocalentregaid("OBSERVADO",currentStoreId).size();
			r3 = repoOperacion.findByEstadoAndLocalentregaid("RECEPCIONADO",currentStoreId).size();
		}else {
			abiertas = repoOperacion.findByEstadoAndLocalpartidaid("ABIERTO",currentStoreId).size();
			cerradas = repoOperacion.findByEstadoAndLocalpartidaid("CERRADO",currentStoreId).size();
			observadas = repoOperacion.findByEstadoAndLocalpartidaid("OBSERVADO",currentStoreId).size();
			recepcionadas = repoOperacion.findByEstadoAndLocalpartidaid("RECEPCIONADO",currentStoreId).size();
			
			r1 = repoDevolucion.findByEstadoAndLocalentregaid("CERRADO",currentStoreId).size();
			r2 = repoDevolucion.findByEstadoAndLocalentregaid("OBSERVADO",currentStoreId).size();
			r3 = repoDevolucion.findByEstadoAndLocalentregaid("RECEPCIONADO",currentStoreId).size();
		}
		
		Integer d1 = abiertas;
		Integer d2 = cerradas;
		Integer d3 = observadas;
		Integer d4 = recepcionadas;

		model.addAttribute("d1",d1);
		model.addAttribute("d2",d2);
		model.addAttribute("d3",d3);
		model.addAttribute("d4",d4);
		
		model.addAttribute("r1",r1);
		model.addAttribute("r2",r2);
		model.addAttribute("r3",r3);
		
		return "index";
	}
	String getRole() {
		
		String s_auth  = SecurityContextHolder.getContext().getAuthentication().getAuthorities().iterator().next().getAuthority();
		return s_auth;
	}
	
	
	@GetMapping("/logout_success")
    public String logout(Model model) {
        return "salir";
	}
	@GetMapping("/out")
    public String out(Model model) {
		//SecurityContextHolder.getContext().getAuthentication().getAuthorities().clear();
		
		//new SecurityContextLogoutHandler().logout(request, null, null);
		
		
		/*
		 Authentication auth = SecurityContextHolder.getContext().getAuthentication(); // concern you

		    User currUser = userService.getUserById(auth.getName()); // some of DAO or Service...

		    SecurityContextLogoutHandler ctxLogOut = new SecurityContextLogoutHandler(); // concern you

		    if( currUser == null ){
		        ctxLogOut.logout(request, response, auth); // concern you
		    }*/
		    
        return "out";
        //return "redirect:/";
	}
	/*
	@GetMapping("/login")
	    public String login(Model model) {
	        return "login";
    }
	@GetMapping("/salir")
    public String logout(Model model) {
        return "salir";
}
	*/
	   
	@GetMapping("/usuarios")
	public String listarUsuarios(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "usuarios");
		return "mant/usuarios";
	}
	


	@GetMapping("/usuario")
	public String usuarioForm(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "usuarios");
		model.addAttribute("create", true);
		model.addAttribute("usuario", new Users());
		
		
		List<Tienda> tiendas = this.repoTienda.findAll();
		model.addAttribute("tiendas", tiendas);
		
		List<Role> roles = this.repoRole.findAll();
		model.addAttribute("lista_roles", roles);
		

		return "mant/usuario";
	}

	@PostMapping("/usuario")
	public String usuarioSubmit(Model model, @ModelAttribute Users usuario) {
		model.addAttribute("create", false);
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "usuarios");

			Users user = repoUsers.findById(usuario.getId()).get();
			user.setTienda(usuario.getTienda());
			repoUsers.save(user);
			model.addAttribute("mensaje", "Usuario actualizado con exito");

		model.addAttribute("create", false);
		repoUsers.save(user);
		return "mant/usuario";
		
	}

	@GetMapping("/usuario/editar/{id}")
	public String usuarioEditar(Model model, @PathVariable("id") String id) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "usuarios");
		Users t = repoUsers.findById(id).get();
		
		List<Tienda> tiendas = this.repoTienda.findAll();
		model.addAttribute("tiendas", tiendas);
		
		List<Role> roles = this.repoRole.findAll();
		model.addAttribute("lista_roles", roles);
		model.addAttribute("create", true);
		model.addAttribute("usuario", t);
		return "mant/usuario";
	}

	@GetMapping("/usuario/eliminar/{id}")
	public String usuarioEliminar(Model model, @PathVariable("id") String id) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "usuarios");
		repoUsers.deleteById(id);
		return "redirect:/usuarios";
	}
	
	@GetMapping("/informacion")
	public String userInfo(Model model, OAuth2AuthenticationToken authentication) {
		model.addAttribute("open_list", "usuario");
		model.addAttribute("menu", "informacion");
		
		
		Users t = this.getByAuth(authentication);
	
		model.addAttribute("tienda_no_especificada",(t.getTienda()==null)?true:false); 

		List<Tienda> tiendas = this.repoTienda.findAll();
		model.addAttribute("tiendas", tiendas);
		model.addAttribute("create", true);
		model.addAttribute("usuario", t);
		return "userinfo";
	}

	

	@GetMapping("/iconos")
	public String iconos(Model model) {
		
		
		return "icons";
	}

	
	
	@GetMapping("/tienda_detalle_stock/{id}")
	public String tiendaDetalleStock(Model model, @PathVariable("id") String id) {
		Tienda tnd = repoTienda.findById(id).get();
		model.addAttribute("tienda", tnd);
		model.addAttribute("open_list", "stock");
		model.addAttribute("menu", "stock_general");
		return "detalle_stock";
	}
	@GetMapping("/tienda")
	public String tiendaForm(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "tiendas");
		model.addAttribute("create", true);
		model.addAttribute("tienda", new Tienda());
		return "mant/tienda";
	}
	@GetMapping("/stock_general")
	public String stockGeneral(Model model) {
		
		List<Tienda> tiendas = repoTienda.findAll();
		for(int p=0;p<tiendas.size();p++) {
			Tienda tienda = tiendas.get(p);
			tienda.updateActivos();
			repoTienda.save(tienda);
		}
		ArrayList<Activo> activos_alquiler = (ArrayList<Activo>) repoActivo.findAll();
		model.addAttribute("activos", activos_alquiler);
		model.addAttribute("open_list", "stock");
		model.addAttribute("menu", "stock_general");
		return "stock_general";
	}
	@PostMapping("/tienda")
	public String tiendaSubmit(Model model, @ModelAttribute Tienda tienda) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "tiendas");
		String id = tienda.getId();
		if (tienda.getId().equals("")) {
			tienda.setId(sequenceGenerator.generateSequence(Tienda.SEQUENCE_NAME)+"");
			model.addAttribute("mensaje", "Tienda creada con exito");
			
			ArrayList<Stock> stocks = new ArrayList<Stock>();
			List<Activo> activos = repoActivo.findAll();
			for(int i=0;i<activos.size();i++) {
				Activo activo = activos.get(i);
				Stock stock = new Stock();
				stock.setSku(activo.getSku());
				stock.setStock(0);
				stocks.add(stock);
			}
			tienda.setStocks(stocks);
			
		} else {
			
			Tienda tnd = repoTienda.findById(tienda.getId()).get();
			tnd.setNombre(tienda.getNombre());
			tnd.setDireccion(tienda.getDireccion());
			tnd.setNumero(tienda.getNumero());
			tnd.setRuc(tienda.getRuc());
			tienda = tnd;
				
			model.addAttribute("id", id);
			model.addAttribute("mensaje", "Tienda actualizada con exito");
		}
		
		model.addAttribute("create", false);
		repoTienda.save(tienda);
		return "mant/tienda";
	}
	@GetMapping("/tienda/editar/{id}")
	public String tiendaEditar(Model model, @PathVariable("id") String id) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "tiendas");
		Tienda t = repoTienda.findById(id).get();
		model.addAttribute("create", true);
		model.addAttribute("tienda", t);
		return "mant/tienda";
	}
	@GetMapping("/tienda/eliminar/{id}")
	public String tiendaEliminar(Model model, @PathVariable("id") String id) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "tiendas");
		repoTienda.deleteById(id);
		return "redirect:/tiendas";
	}
	
	
	
	
	
	
	@GetMapping("/transporte")
	public String transporteForm(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "transportes");
		model.addAttribute("create", true);
		model.addAttribute("transporte", new Transporte());
		return "mant/transporte";
	}

	@GetMapping("/transporte/editar/{id}")
	public String transporteEditar(Model model, @PathVariable("id") String id) {
		Transporte t = repoTransporte.findById(id).get();
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "transportes");
		model.addAttribute("create", true);
		model.addAttribute("transporte", t);
		return "mant/transporte";
	}
	@GetMapping("/transporte/eliminar/{id}")
	public String transporteEliminar(Model model, @PathVariable("id") String id) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "transportes");
		repoTransporte.deleteById(id);
		return "redirect:/transportes";
	}
	@PostMapping("/transporte")
	public String transporteSubmit(Model model, @ModelAttribute Transporte transporte) {
		if (transporte.getId() == null) {
			transporte.setId(sequenceGenerator.generateSequence(Transporte.SEQUENCE_NAME)+"");
			model.addAttribute("mensaje", "Transporte creado con exito");
		} else {
			model.addAttribute("mensaje", "Transporte actualizado con exito");
		}
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "transportes");
		repoTransporte.save(transporte);
		model.addAttribute("create", false);
		return "mant/transporte";
	}

	@GetMapping("/activo")
	public String activoForm(Model model) {
		model.addAttribute("open_list", "stock");
		model.addAttribute("menu", "activos");
		model.addAttribute("create", true);
		model.addAttribute("activo", new Activo());
		return "mant/activo";
	}

	@GetMapping("/activo/editar/{id}")
	public String activoEditar(Model model, @PathVariable("id") String id) {
		Activo t = repoActivo.findById(id).get();
		model.addAttribute("open_list", "stock");
		model.addAttribute("menu", "activos");
		model.addAttribute("create", true);
		model.addAttribute("activo", t);
		return "mant/activo";
	}
	private boolean checkIfSKUExist(ArrayList<Stock> stocks,String SKU) {
		if(stocks!=null) {
			for(int i=0;i<stocks.size();i++) {
				if(stocks.get(i).getSku().equals(SKU)) {
					return true;
				}
			}
		}
		
		return false;
	}
	@PostMapping("/activo")
	public String activoSubmit(Model model, @ModelAttribute Activo activo) {
		model.addAttribute("open_list", "stock");
		model.addAttribute("menu", "activos");
		if (activo.getId() == null) {
			activo.setId(sequenceGenerator.generateSequence(Activo.SEQUENCE_NAME)+"");
			model.addAttribute("mensaje", "Activo creado con exito");
		} else {
			
			model.addAttribute("mensaje", "Activo actualizado con exito");
		}
		List<Tienda> tiendas = repoTienda.findAll();
		for(int i=0;i<tiendas.size();i++) {
			Tienda tienda = tiendas.get(i);
			Stock stock = new Stock();
			stock.setSku(activo.getSku());
			stock.setStock(0);			
			if(!checkIfSKUExist(tienda.getStocks(),activo.getSku())) {
				if(tienda.getStocks()==null) {
					tienda.setStocks(new ArrayList<Stock>());
				}
				tienda.getStocks().add(stock);
				repoTienda.save(tienda);
			}
		}
		activo.setFecha_registro(new Date());
		repoActivo.save(activo);
		model.addAttribute("create", false);
		return "mant/activo";
	}
	@PostMapping("/activo_alquiler")
	public String activoAlquilerSubmit(Model model, @ModelAttribute Activo activo) {
		model.addAttribute("open_list", "stock");
		model.addAttribute("menu", "activos");
		if (activo.getId() == null) {
			activo.setId(sequenceGenerator.generateSequence(Activo.SEQUENCE_NAME)+"");
			model.addAttribute("mensaje", "Activo creado con exito");
		} else {
			model.addAttribute("mensaje", "Activo actualizado con exito");
		}
		repoActivo.save(activo);
		model.addAttribute("create", false);
		return "redirect:/alquiler_activos";
	}
	@GetMapping("/activo/eliminar/{id}")
	public String activoEliminar(Model model, @PathVariable("id") String id) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "activos");
		
		Activo activo = repoActivo.findById(id).get();
		List<Tienda> tiendas = repoTienda.findAll();
		
		for(int i=0;i<tiendas.size();i++) {
			Tienda tienda = tiendas.get(i);
			ArrayList<Stock> stocks = tienda.getStocks();
			for(int p=0;p<stocks.size();p++) {
				if(stocks.get(p).getSku().equals(activo.getSku())) {
					tienda.getStocks().remove(p);
				}
			}
			repoTienda.save(tienda);
		}

		repoActivo.deleteById(id);
		return "redirect:/activos";
	}
	
	
	@GetMapping("/calculo_rebate")
	public String calculoRebate(Model model) {
		model.addAttribute("activos", getActivosAlquiler());
		model.addAttribute("menu", "calculo_rebate");
		repoReporteProveedor.deleteAll();
		sequenceGenerator.resetSequence(ReporteProveedor.SEQUENCE_NAME);
		return "rebate";
	}
	

	@GetMapping("/proveedor")
	public String proveedorForm(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "proveedores");
		model.addAttribute("create", true);
		//ArrayList<ActivoDetalleProveedor> detalle = new ArrayList<ActivoDetalleProveedor>(); 
		Proveedor prov = new Proveedor();
		/*ArrayList<Activo> activos_alquiler = (ArrayList<Activo>) repoActivo.findByAlquiler(true);
		for(int i=0;i<activos_alquiler.size();i++){
			ActivoDetalleProveedor detalle_proveedor = new ActivoDetalleProveedor();
			detalle_proveedor.setCantidad(0);
			detalle_proveedor.setSku(activos_alquiler.get(i).getSku());
			detalle_proveedor.setNombre(activos_alquiler.get(i).getNombre());
			detalle.add(detalle_proveedor);
		}
		prov.setDetalle(detalle);*/
		model.addAttribute("proveedor", prov);
		return "mant/proveedor";
	}

	@GetMapping("/proveedor/editar/{id}")
	public String proveedorEditar(Model model, @PathVariable("id") String id) {
		Proveedor prov = repoProveedor.findById(id).get();
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "proveedores");
		model.addAttribute("create", true);
		
		
		ArrayList<ActivoDetalleProveedor> detalle = prov.getDetalle();
		if(detalle==null) {
			detalle = new ArrayList<ActivoDetalleProveedor>(); 
			prov.setDetalle(detalle);
		}
		ArrayList<Activo> activos_alquiler = (ArrayList<Activo>) repoActivo.findByAlquiler(true);
		for(int i=0;i<activos_alquiler.size();i++){
			boolean flag = false;
			for(int p=0;p<detalle.size();p++){
				if(detalle.get(p).getSku().equals(activos_alquiler.get(i).getSku())) {
					flag=true;
					break;
				}
			}
			if(!flag) {
				ActivoDetalleProveedor detalle_proveedor = new ActivoDetalleProveedor();
				detalle_proveedor.setCantidad(0);
				detalle_proveedor.setSku(activos_alquiler.get(i).getSku());
				detalle_proveedor.setNombre(activos_alquiler.get(i).getNombre());
				prov.getDetalle().add(detalle_proveedor);
			}
			
		}
		model.addAttribute("proveedor", prov);
		return "mant/proveedor";
	}
	@GetMapping("/proveedor/eliminar/{id}")
	public String proveedorEliminar(Model model, @PathVariable("id") String id) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "proveedores");
		repoProveedor.deleteById(id);
		return "redirect:/proveedores";
	}

	@PostMapping("/proveedor")
	public String proveedorSubmit(Model model, @ModelAttribute Proveedor proveedor) {

		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "proveedores");

		if (proveedor.getId().equals("")) {
			proveedor.setId(sequenceGenerator.generateSequence(Proveedor.SEQUENCE_NAME)+"");
			repoProveedor.save(proveedor);
			model.addAttribute("mensaje", "Proveedor creado con exito");
		} else {
			Proveedor p = repoProveedor.findById(proveedor.getId()).get();
			p.setRazon_social(proveedor.getRazon_social());
			p.setRuc(proveedor.getRuc());
			p.setDetalle(proveedor.getDetalle());
			repoProveedor.save(p);
			model.addAttribute("mensaje", "Proveedor actualizado con exito");
		}
		
		model.addAttribute("create", false);
		return "mant/proveedor";
	}
	
	
	
	
	
	
	@GetMapping("/proveedor_activo")
	public String proveedorActivoForm(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "proveedores_activos");
		model.addAttribute("create", true);
		model.addAttribute("proveedor", new ProveedorActivo());
		return "mant/proveedor_activo";
	}

	@GetMapping("/proveedor_activo/editar/{id}")
	public String proveedorActivoEditar(Model model, @PathVariable("id") String id) {
		ProveedorActivo t = repoProveedorActivo.findById(id).get();
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "proveedores_activos");
		model.addAttribute("create", true);
		model.addAttribute("proveedor", t);
		return "mant/proveedor_activo";
	}
	@GetMapping("/proveedor_activo/eliminar/{id}")
	public String proveedorActivoEliminar(Model model, @PathVariable("id") String id) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "proveedores_activos");
		repoProveedorActivo.deleteById(id);
		return "redirect:/proveedores_activos";
	}

	@PostMapping("/proveedor_activo")
	public String proveedorActivoSubmit(Model model, @ModelAttribute ProveedorActivo proveedor) {

		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "proveedores_activos");

		if (proveedor.getId().equals("")) {
			proveedor.setId(sequenceGenerator.generateSequence(ProveedorActivo.SEQUENCE_NAME)+"");
			repoProveedorActivo.save(proveedor);
			model.addAttribute("mensaje", "Proveedor creado con exito");
		} else {
			ProveedorActivo p = repoProveedorActivo.findById(proveedor.getId()).get();
			p.setRazon_social(proveedor.getRazon_social());
			p.setRuc(proveedor.getRuc());
			repoProveedorActivo.save(p);
			model.addAttribute("mensaje", "Proveedor actualizado con exito");
		}
		
		model.addAttribute("create", false);
		return "mant/proveedor_activo";
	}
	
	
	
	
	
	/*
	@GetMapping("/cliente")
	public String clienteForm(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "clientes");
		model.addAttribute("create", true);
		model.addAttribute("cliente", new Cliente());
		return "mant/cliente";
	}

	@GetMapping("/cliente/editar/{id}")
	public String clienteEditar(Model model, @PathVariable("id") String id) {
		Cliente t = repoCliente.findById(id).get();
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "clientes");
		model.addAttribute("create", true);
		model.addAttribute("cliente", t);
		return "mant/cliente";
	}
	
	@PostMapping("/cliente")
	public String clienteSubmit(Model model, @ModelAttribute Cliente cliente) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "clientes");
		if (cliente.getId() == null) {
			model.addAttribute("mensaje", "Cliente creado con exito");
		} else {
			model.addAttribute("mensaje", "Cliente actualizado con exito");
		}
		repoCliente.save(cliente);
		model.addAttribute("create", false);
		return "mant/cliente";
	}
	*/
	
	

	@GetMapping("/compra")
	public String compraForm(Model model) {
		List<ProveedorActivo> proveedores = this.repoProveedoresActivos.findAll();
		model.addAttribute("proveedores", proveedores);
		List<Activo> activos = this.repoActivo.findAll();
		model.addAttribute("activos", activos);
		
		model.addAttribute("view_mode", false);
		model.addAttribute("open_list", "stock");
		model.addAttribute("menu", "compras");
		model.addAttribute("create", true);
		model.addAttribute("compra", new Compra());
		return "compra";
	}
	@GetMapping("/compra/ver/{id}")
	public String compraVer(Model model, @PathVariable("id") String id) {
	
		Compra t = repoCompra.findById(id).get();
		
		List<Proveedor> proveedores = this.repoProveedores.findAll();
		model.addAttribute("proveedores", proveedores);
		
		List<Activo> activos = this.repoActivo.findAll();
		model.addAttribute("activos", activos);
		model.addAttribute("view_mode",true);
		
		model.addAttribute("proveedor_id",t.getProveedor().getId());
		model.addAttribute("total_activos", t.getCantidad_activos());
		model.addAttribute("menu", "compras");
		model.addAttribute("create", true);
		model.addAttribute("compra", t);
		model.addAttribute("titulo","Compra de Unidades");
		
		
		return "compra";
	}
	@GetMapping("/compra/eliminar/{id}")
	public String compraEliminar(Model model, @PathVariable("id") String id) {
		model.addAttribute("menu", "compras");
		repoCompra.deleteById(id);
		return "redirect:/compras";
	}

	@PostMapping("/compra")
	public String compraSubmit(Map<String, Object> model, @Valid Compra operacion, BindingResult result) {
		System.out.print("*compraSubmit *****");
		List<Proveedor> proveedores = this.repoProveedores.findAll();
		model.put("proveedores", proveedores);
		List<Activo> activos = this.repoActivo.findAll();
		model.put("activos", activos);
		System.out.print("*1");
		
		if (result.hasErrors()) {
			List<ObjectError> errores = result.getAllErrors();
			for (int i = 0; i < errores.size(); i++) {
				System.out.println(errores.get(i).getDefaultMessage());
			}
			return "compra";
		}
		System.out.print("*2");
		String id = operacion.getId();
		if (id.equals("")) {
			operacion.setId(sequenceGenerator.generateSequence(Compra.SEQUENCE_NAME)+"");
			List<ActivoDetalle> activo_detalle = operacion.getActivos();
			Tienda tienda = getCurrentTienda();
			for (int i = 0; i < activo_detalle.size(); i++) {
				ActivoDetalle ad = activo_detalle.get(i);
				Integer stock = tienda.getStockBySKU(ad.getSku()) + ad.getCantidad();
				tienda.setStockBySKU(ad.getSku(), stock);
			}
			this.repoTienda.save(tienda);
			
			model.put("mensaje", "Compra realizada con exito");
			operacion.setFecha_registro(new Date());
		} else {
			Compra op = repoCompra.findById(operacion.getId()).get();
			operacion = op;
			model.put("mensaje", "Compra actualizado con exito");
		}
		System.out.print("*3");
		operacion.setNombre_proveedor(operacion.getProveedor().getRazon_social());
		Integer cantidad = 0;
		for(int i=0;i<operacion.getActivos().size();i++) {
			cantidad+= operacion.getActivos().get(i).getCantidad();
		}
		System.out.print("*4");
		operacion.setCantidad_activos(cantidad);
		repoCompra.save(operacion);
		model.put("open_list", "stock");
		model.put("menu", "compras");
		model.put("view_mode",false);
		model.put("create", false);
		return "compra";
	}

	
	
	
	
	
	@GetMapping("/stock")
	public String stock(Model model) {
		Stocksave booksForm = new Stocksave();
		List<Activo> activos = this.repoActivo.findAll();
		Tienda tienda = getCurrentTienda();
	    for (int i = 0; i < activos.size(); i++) {
	    	Activo activo = activos.get(i);
	    	
	    	ActivoRef ref = new ActivoRef();
	    	ref.setSku(activo.getSku());
	    	ref.setStock(tienda.getStockBySKU(activo.getSku()));
	    	ref.setNombre(activo.getNombre());

	        booksForm.addActivo(ref);
	    }
	    
	    if(getRole().equals("ROLE_SUULL_TIENDA")) {
	    	model.addAttribute("viewmode", true);
	    }else {
	    	model.addAttribute("viewmode", false);
	    }
	    
	    model.addAttribute("stocksave", booksForm);
		model.addAttribute("open_list", "stock");
		model.addAttribute("menu", "stock");
		return "stock";
	}
	
	@GetMapping("/reporte")
	public String reporte(Model model) {
		return "reporte";
	}
	
	
	
	@PostMapping("/stock")
	public String stockSubmit(Model model, @ModelAttribute Stocksave stocksave) {
		Tienda tienda = getCurrentTienda();
		tienda.stocks.clear();
		for (int i = 0; i < stocksave.getActivos().size(); i++) {
			ActivoRef activo = stocksave.getActivos().get(i);
			tienda.setStockBySKU(activo.getSku(), activo.getStock());
			Stock s = new Stock();
			s.setSku(activo.getSku());
			s.setStock(activo.getStock());
			tienda.stocks.add(s);
	    }
		tienda.updateActivos();
		repoTienda.save(tienda);
		return "redirect:/stock";
	}
	
	
	@GetMapping("/alquiler_activo/editar/{id}")
	public String alquilerActivoEditar(Model model, @PathVariable("id") String id) {
		/*Activo activo = this.repoActivo.findById(id).get();
		activo.setAlquiler(true);
		this.repoActivo.save(activo);
		return "redirect:/alquiler_activos";
		*/
		Activo t = repoActivo.findById(id).get();
		//model.addAttribute("open_list", "stock");
		model.addAttribute("menu", "alquiler_activos");
		model.addAttribute("create", true);
		model.addAttribute("activo", t);
		return "mant/alquiler_activo";
	}
	@GetMapping("/alquiler_activo/activar/{id}")
	public String alquilerActivoActivar(Model model, @PathVariable("id") String id) {
		Activo activo = this.repoActivo.findById(id).get();
		activo.setAlquiler(true);
		activo.setPrecio(0.0);
		activo.setPrecio_no_lavado(0.0);
		this.repoActivo.save(activo);
		return "redirect:/alquiler_activos";
	}
	@GetMapping("/alquiler_activo/desactivar/{id}")
	public String alquilerActivoDesactivar(Model model, @PathVariable("id") String id) {
		Activo activo = this.repoActivo.findById(id).get();
		activo.setAlquiler(false);
		this.repoActivo.save(activo);
		return "redirect:/alquiler_activos";
	}
	@RequestMapping(value = "/alquiler_activos", method = RequestMethod.GET)
	public String listAlquilerActivos(Model model) {
		model.addAttribute("menu", "alquiler_activos");
		return "mant/alquiler_activos";
	}
	
	@RequestMapping(value = "/activos", method = RequestMethod.GET)
	public String listActivos(Model model) {
		model.addAttribute("open_list", "stock");
		model.addAttribute("menu", "activos");
		return "mant/activos";
	}

	@RequestMapping(value = "/tiendas", method = RequestMethod.GET)
	public String listTiendas(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "tiendas");
		return "mant/tiendas";
	}

	@RequestMapping(value = "/clientes", method = RequestMethod.GET)
	public String listClientes(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "clientes");
		return "mant/clientes";
	}

	@RequestMapping(value = "/transportes", method = RequestMethod.GET)
	public String listTransportes(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "transportes");
		return "mant/transportes";
	}

	@RequestMapping(value = "/proveedores", method = RequestMethod.GET)
	public String listProveedores(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "proveedores");
		return "mant/proveedores";
	}
	@RequestMapping(value = "/proveedores_activos", method = RequestMethod.GET)
	public String listProveedoresActivos(Model model) {
		model.addAttribute("open_list", "configuracion");
		model.addAttribute("menu", "proveedores_activos");
		return "mant/proveedores_activos";
	}
	@RequestMapping(value = "/proveedores_stock", method = RequestMethod.GET)
	public String listProveedoresStock(Model model) {
		model.addAttribute("activos", getActivosAlquiler());
		model.addAttribute("menu", "proveedores_stock");
		return "proveedores_stock";
	}
	private ArrayList<Activo> getActivosAlquiler(){
		ArrayList<Activo> activos_alquiler = (ArrayList<Activo>) repoActivo.findByAlquiler(true);
		return activos_alquiler;
	}
	


	@RequestMapping(value = "/recepciones", method = RequestMethod.GET)
	public String listRecepciones(Model model) {
		model.addAttribute("menu", "recepciones");
		return "recepciones";
	}

	@RequestMapping(value = "/compras", method = RequestMethod.GET)
	public String listCompras(Model model) {
		model.addAttribute("open_list", "stock");
		model.addAttribute("menu", "compras");
		return "compras";
	}

	

	@RequestMapping(value = "/users/mysql", method = RequestMethod.GET)
	public String listUsers(Model model) {
		return "usuarios";
	}

	private Tienda getCurrentTienda() {
		DefaultOidcUser user = (DefaultOidcUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return repoUsers.findById(user.getAttribute("oid")).get().getTienda();
	}
}
