package com.tottus.suull.entity;

public class Reporte1 implements Reporte {
	
	
	public Reporte1(String id, String nombres, String apellidos, String tipo_doc, String documento,
			String correo_electronico, String celular, String colaborador, String segmento_comercial,
			String ramos_vigentes, String canal_venta, String contrat_product_aseg_benef, String ingresos,
			String fecha_creacion) {
		super();
		this.id = id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.tipo_doc = tipo_doc;
		this.documento = documento;
		this.correo_electronico = correo_electronico;
		this.celular = celular;
		this.colaborador = colaborador;
		this.segmento_comercial = segmento_comercial;
		this.ramos_vigentes = ramos_vigentes;
		this.canal_venta = canal_venta;
		this.contrat_product_aseg_benef = contrat_product_aseg_benef;
		this.ingresos = ingresos;
		this.fecha_creacion = fecha_creacion;
	}

	public static String[] header = {	"id_usuario",
										"Nombres",
										"Apellidos",
										"Tipo Doc",
										"Nro. Doc",
										"Correo",
										"Celular",
										"Colaborador",
										"Segmento Comercial",
										"Ramos Vigentes",
										"Canal de venta",
										"contrat_product_aseg_benef",
										"Ingresos",
										"Fecha de Registro"};
	

	private String id;
	private String nombres;
	private String apellidos;
	private String tipo_doc;
	private String documento;
	private String correo_electronico;
	private String celular;
	private String colaborador;
	private String segmento_comercial;
	private String ramos_vigentes;
	private String canal_venta;
	private String contrat_product_aseg_benef;
	private String ingresos;
	private String fecha_creacion;

	@Override
	public String[] getAll() {
		String[] response = {
				this.id,
				this.nombres,
				this.apellidos,
				this.tipo_doc,
				this.documento,
				this.correo_electronico,
				this.celular,
				this.colaborador,
				this.segmento_comercial,
				this.ramos_vigentes,
				this.canal_venta,
				this.contrat_product_aseg_benef,
				this.ingresos,
				this.fecha_creacion
		};
		return response;
	}
	
	
	
}
