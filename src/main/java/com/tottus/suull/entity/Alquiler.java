package com.tottus.suull.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "alquiler")
public class Alquiler {
	
	@Transient
    public static final String SEQUENCE_NAME = "alquiler_sequence";
	
	@Id
    private String id;
	
	private String nro_operacion;
	
	private String usuario;
	
	@DBRef
	private Proveedor proveedor;
	
	@DBRef(lazy = true)
    private List<DetalleAlquiler> detalle;
	
	private String proveedor_nombre;
	
	private String operacion;
	
	private String estado;
	
	private Integer total;
	
	private Double costo;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha_alquiler;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha_devolucion;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha;
	
	
}
