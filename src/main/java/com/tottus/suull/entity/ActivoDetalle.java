package com.tottus.suull.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
//@Document(collection = "activo_detalle")
public class ActivoDetalle {
	
    private String id;
	
	private String sku;
	
	private String nombre;
	
	private Integer cantidad;
	
	/*
	@DBRef
	 private Activo activo;
	
	
	public void setSku(String sku) {
		this.activo = repoActivo.findBySku(sku);
	}*/
}
