package com.tottus.suull.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "activo")
public class Activo {
	
	@Transient
    public static final String SEQUENCE_NAME = "activo_sequence";
	
	@Id
    private String id;
	
	private String sku;
	
	private String nombre;
	
	private String tipo;
	
	private boolean alquiler;
	
	private Integer pos;
	
	private Integer stock;
	
	private Double precio;
	private Double precio_no_lavado;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha_registro;
	
	private String unidad;
	
}
