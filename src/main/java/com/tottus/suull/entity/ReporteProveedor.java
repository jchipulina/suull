package com.tottus.suull.entity;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "reporte_proveedor")
public class ReporteProveedor {
	
	@Transient
    public static final String SEQUENCE_NAME = "reporte_proveedor_sequence";
	
	@Id
    private String id;
	
	private String ruc_proveedor;
	private String nombre_proveedor;
	
	private String usuario;
	
	private ArrayList<RespCreateMonto> montos;

	private Date fecha_inicio;
	private Date fecha_fin;
	/*
	public String[] getAll() {
		String[] response = {
				this.id,
				this.ruc_proveedor,
				this.nombre_proveedor,
				""+this.monto_1,
				""+this.monto_2,
				""+this.monto_3,
				""+this.monto_4,
				this.fecha_inicio.toString(),
				this.fecha_fin.toString(),
		};
		return response;
	}
	*/
	
}