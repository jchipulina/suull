package com.tottus.suull.entity;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "compra")
public class Compra {
	
	@Transient
    public static final String SEQUENCE_NAME = "compra_sequence";
	
	@Id
    private String id;
	
	@DBRef
	private ProveedorActivo proveedor;
	
	private String nombre_proveedor;
	
	private String guia_remision;
	private String observaciones;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha_registro;
	
	private Integer cantidad_activos;
	
	private ArrayList<ActivoDetalle> activos = new  ArrayList<ActivoDetalle>();
	
	public ArrayList<ActivoDetalle> getActivos() {
		return activos;
	}
	public void setActivos(ArrayList<ActivoDetalle> activos) {
		this.activos = activos;
	}
	
	
	
	
	
	
	
}
