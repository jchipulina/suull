package com.tottus.suull.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "reporte_movimientos")
public class ReporteMovimientos {
	
	@Transient
    public static final String SEQUENCE_NAME = "reporte_movimientos_sequence";
	@Id
    private String id;
	private String nro_operacion;
	private String tipo_movimiento;
	private String tipo_movimiento_final;
	private String origen_loc;
	private String destino_loc;
	private String sku;
	private String oh_origen;
	private String oh_ingreso;
	private String oh_salida;
	private String oh_discrepancia;
	private String nro_operacion_origen;
	private Date fecha_emision;
	private Date fecha_recepcion;
	private String observacion;
	private String ruc_emptransp;
	private String razon_social;
	private String nro_licencia;
	private String nom_chofer;
	private String marca;
	private String nro_placa;
	private String nro_guia_remision;
	private String nro_precinto;
	private Users usuario;
	
}