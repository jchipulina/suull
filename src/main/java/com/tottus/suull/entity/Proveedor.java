package com.tottus.suull.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Id;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.tottus.suull.config.SpringConfiguration;
import com.tottus.suull.repository.RepoActivo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "proveedor")
public class Proveedor {
	
	@Transient
    public static final String SEQUENCE_NAME = "proveedor_sequence";
	@Id
    private String id;
	private String ruc;
	private String razon_social;

	private ArrayList<ActivoDetalleProveedor> detalle;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha_registro;
	
	public Double getCosto_diario() {
		
		Double costo_diario = 0.0;
		RepoActivo repoActivo = (RepoActivo) SpringConfiguration.contextProvider().getApplicationContext().getBean("repoActivo");
		List<Activo> lista = repoActivo.findAll();
		for(int i=0;i<lista.size();i++) {
			Activo activo = lista.get(i);
			if(detalle!=null) {
				for(int p=0;p<detalle.size();p++) {
					if(detalle.get(p).getSku().equals(activo.getSku())) {
						Double precio = activo.getPrecio() * detalle.get(p).getCantidad();
						costo_diario+=precio;
						break;
					}
				}
			}
		}
		return costo_diario;
	}
}
