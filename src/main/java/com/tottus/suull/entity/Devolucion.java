package com.tottus.suull.entity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "devolucion")
public class Devolucion{
	
	@Transient
    public static final String SEQUENCE_NAME = "devolucion_sequence";
	
	@Id
    private String id;
	
	private String nro_operacion;
	
	@NotEmpty(message="La guía de remision es obligatoria")
	private String guia_remision;
	
	private String observaciones;
	
	@DBRef
	private Users usuario;
	
	private String observacion_tienda;
	
	private String estado;
	
	private Boolean transferencia_pura;
	
	private String local_partida_id;
	private String local_entrega_id;

	@NotNull(message="La fecha de inicio de traslado es obligatoria")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha_inicio_traslado;

	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date fecha_registro;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date fecha_recepcion;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date fecha_actualizacion;
	
	private Integer cantidad_activos;
	
	private ArrayList<ActivoDetalle> activos = new  ArrayList<ActivoDetalle>();
	
	@DBRef
	 private Transporte transporte;
	@DBRef
 	 private Tienda local_partida;
	@DBRef
	 private Tienda local_entrega;

	private String nombre_local_entrega;
	private String nombre_local_partida;
	public ArrayList<ActivoDetalle> getActivos() {
		return activos;
	}
	public void setActivos(ArrayList<ActivoDetalle> activos) {
		this.activos = activos;
	}
	

}
