package com.tottus.suull.entity;

import java.util.Date;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "users")
public class Users {
	
	@Id
	 private String id;
  	 
	 private String rol;
  	 
  	 @DBRef
  	 private Tienda tienda;
  	 private String nombres;
  	 private String username;
 	 private String email;
 	 private Date fecha_creacion;
 	 
 	 public String getNombre_completo() {
 		 return nombres;
 	 }
 	 public String getLocal() {
 		 if(tienda==null)return "-";
		 return tienda.getNombre();
	 }
 	 public String getAcceso() {
		 return rol;
	 }
}