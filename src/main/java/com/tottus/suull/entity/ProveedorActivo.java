package com.tottus.suull.entity;

import java.util.Date;

import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.tottus.suull.config.SpringConfiguration;
import com.tottus.suull.repository.RepoPrecio;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "proveedor_activo")
public class ProveedorActivo {
	
	@Transient
    public static final String SEQUENCE_NAME = "proveedor_activo_sequence";
	@Id
    private String id;
	
	private String ruc;
	private String razon_social;
	
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha_registro;
	
}
