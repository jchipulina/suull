package com.tottus.suull.entity;

import java.util.Date;

import javax.persistence.Id;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "precio")
public class Precio {
	
	@Transient
    public static final String SEQUENCE_NAME = "precio_sequence";
	
	@Id
    private String id;
	
	private Double tipo_1;
	private Double tipo_2;
	private Double tipo_3;
	private Double tipo_4;
	
	private String descripcion_1;
	private String descripcion_2;
	private String descripcion_3;
	private String descripcion_4;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha_actualizacion;
	
	
}
