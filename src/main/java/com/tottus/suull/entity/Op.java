package com.tottus.suull.entity;

import java.util.ArrayList;
import java.util.Date;

public class Op {
	public String id="";
    public String nro_operacion="";
	public String guia_remision="";
	public String observaciones="";
	public Users usuario=null;
	public String observacion_tienda="";
	public String estado="";
	public Boolean transferencia_pura=false;
	public String local_partida_id="";
	public String local_entrega_id="";
	public Date fecha_inicio_trasladon=null;
	public Date fecha_registro=null;
	public Date fecha_recepcion=null;
	public Date fecha_actualizacion=null;
	public Integer cantidad_activos=0;
	public ArrayList<ActivoDetalle> activos = new ArrayList<ActivoDetalle>();
	public Transporte transporte=null;
    public Tienda local_partida=null;
 	public Tienda local_entrega=null;
	public String nombre_local_entrega="";
	public String nombre_local_partida="";
	//public ArrayList<ActivoDetalle> getActivos=new ArrayList<ActivoDetalle>();
	//public void setActivos(ArrayList<ActivoDetalle> activos);
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNro_operacion() {
		return nro_operacion;
	}
	public void setNro_operacion(String nro_operacion) {
		this.nro_operacion = nro_operacion;
	}
	public String getGuia_remision() {
		return guia_remision;
	}
	public void setGuia_remision(String guia_remision) {
		this.guia_remision = guia_remision;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Users getUsuario() {
		return usuario;
	}
	public void setUsuario(Users usuario) {
		this.usuario = usuario;
	}
	public String getObservacion_tienda() {
		return observacion_tienda;
	}
	public void setObservacion_tienda(String observacion_tienda) {
		this.observacion_tienda = observacion_tienda;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Boolean getTransferencia_pura() {
		return transferencia_pura;
	}
	public void setTransferencia_pura(Boolean transferencia_pura) {
		this.transferencia_pura = transferencia_pura;
	}
	public String getLocal_partida_id() {
		return local_partida_id;
	}
	public void setLocal_partida_id(String local_partida_id) {
		this.local_partida_id = local_partida_id;
	}
	public String getLocal_entrega_id() {
		return local_entrega_id;
	}
	public void setLocal_entrega_id(String local_entrega_id) {
		this.local_entrega_id = local_entrega_id;
	}
	public Date getFecha_inicio_trasladon() {
		return fecha_inicio_trasladon;
	}
	public void setFecha_inicio_trasladon(Date fecha_inicio_trasladon) {
		this.fecha_inicio_trasladon = fecha_inicio_trasladon;
	}

	public Date getFecha_actualizacion() {
		return fecha_actualizacion;
	}
	public void setFecha_actualizacion(Date fecha_actualizacion) {
		this.fecha_actualizacion = fecha_actualizacion;
	}
	public Integer getCantidad_activos() {
		return cantidad_activos;
	}
	public void setCantidad_activos(Integer cantidad_activos) {
		this.cantidad_activos = cantidad_activos;
	}
	public ArrayList<ActivoDetalle> getActivos() {
		return activos;
	}
	public void setActivos(ArrayList<ActivoDetalle> activos) {
		this.activos = activos;
	}
	public Transporte getTransporte() {
		return transporte;
	}
	public void setTransporte(Transporte transporte) {
		this.transporte = transporte;
	}
	public Tienda getLocal_partida() {
		return local_partida;
	}
	public void setLocal_partida(Tienda local_partida) {
		this.local_partida = local_partida;
	}
	public Tienda getLocal_entrega() {
		return local_entrega;
	}
	public void setLocal_entrega(Tienda local_entrega) {
		this.local_entrega = local_entrega;
	}
	public String getNombre_local_entrega() {
		return nombre_local_entrega;
	}
	public void setNombre_local_entrega(String nombre_local_entrega) {
		this.nombre_local_entrega = nombre_local_entrega;
	}
	public String getNombre_local_partida() {
		return nombre_local_partida;
	}
	public void setNombre_local_partida(String nombre_local_partida) {
		this.nombre_local_partida = nombre_local_partida;
	}
	public Date getFecha_registro() {
		return fecha_registro;
	}
	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}
	public Date getFecha_recepcion() {
		return fecha_recepcion;
	}
	public void setFecha_recepcion(Date fecha_recepcion) {
		this.fecha_recepcion = fecha_recepcion;
	}
}