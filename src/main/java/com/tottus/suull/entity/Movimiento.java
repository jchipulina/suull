package com.tottus.suull.entity;

import java.util.Date;

import javax.persistence.Id;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "movimiento")
public class Movimiento {
	
	@Transient
    public static final String SEQUENCE_NAME = "movimiento_sequence";
	
	@Id
    private String id;
	
    private Integer cantidad;
    
    private Integer devuelto;
    
    private Double precio;
    
    private Integer stock;
    
    private String usuario;

    
    @DBRef
    private Activo activo;
	
	@DBRef
	private Proveedor proveedor;
	
	@DBRef
	private Alquiler alquiler;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date fecha;

	
}
