package com.tottus.suull.entity;

import java.util.ArrayList;


public class Stocksave {

	private ArrayList<ActivoRef> activos = new  ArrayList<ActivoRef>();
	
	public void addActivo(ActivoRef book) {
        this.activos.add(book);
    }

	public ArrayList<ActivoRef> getActivos() {
		return activos;
	}

	public void setActivos(ArrayList<ActivoRef> activos) {
		this.activos = activos;
	}
	

}
