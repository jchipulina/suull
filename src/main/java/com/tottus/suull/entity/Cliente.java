package com.tottus.suull.entity;

import java.util.Date;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "cliente")
public class Cliente {
	
	@Id
    private String id;
	
	private String ruc;
	private String razon_social;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha_registro;
	
	
}
