package com.tottus.suull.entity;

import java.util.List;

import javax.persistence.Id;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "alquiler_detalle")
public class DetalleAlquiler {
	
	@Transient
    public static final String SEQUENCE_NAME = "alquiler_detalle_sequence";
	@Id
    private String id;
	private String sku;
	private String nombre;
	private Integer cantidad;
	private Double precio;
	private Double precio_no_lavado;
	private boolean no_lavado;
	
	@DBRef(lazy = true)
    private List<Alquiler> alquiler_ids;

}
