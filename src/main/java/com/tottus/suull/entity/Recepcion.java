package com.tottus.suull.entity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "recepcion")
public class Recepcion {
	
	@Transient
    public static final String SEQUENCE_NAME = "recepcion_sequence";
	
	@Id
    private String id;
	
	private String nro_operacion;
	
	@NotEmpty(message="La guía de remision es obligatoria")
	private String guia_remision;
	
	private String observaciones;
	
	private String usuario;
	
	private String estado = "ABIERTO";

	@NotNull(message="La fecha de inicio de traslado es obligatoria")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date fecha_inicio_traslado;

	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date fecha_registro;
	
	private Integer cantidad_activos = 0;
	
	private ArrayList<ActivoDetalle> activos = new  ArrayList<ActivoDetalle>();
	
	private Transporte transporte; 
	private Tienda local_partida; 
	private Tienda local_entrega; 
	
	
	
	public ArrayList<ActivoDetalle> getActivos() {
		return activos;
	}
	public void setActivos(ArrayList<ActivoDetalle> activos) {
		this.activos = activos;
	}
	
	public String getRuc_transporte() {
		return this.transporte.getRuc();
	}
	public String getNombre_local_partida() {
		return this.local_partida.getNombre();
	}
	public String getNombre_local_entrega() {
		return this.local_entrega.getNombre();
	}
	public String getFecha_inicio_traslado_f() {
		if(fecha_registro!=null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			return format.format( fecha_inicio_traslado );
		}
		return "";
	}
	public String getFecha_registro_f() {
		if(fecha_registro!=null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");
			return format.format( fecha_registro );
		}
		return "";
	}
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNro_operacion() {
		return nro_operacion;
	}

	public void setNro_operacion(String nro_operacion) {
		this.nro_operacion = nro_operacion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public Date getFecha_inicio_traslado() {
		return fecha_inicio_traslado;
	}

	public void setFecha_inicio_traslado(Date fecha_inicio_traslado) {
		this.fecha_inicio_traslado = fecha_inicio_traslado;
	}

	public Date getFecha_registro() {
		return fecha_registro;
	}

	public void setFecha_registro(Date fecha_registro) {
		this.fecha_registro = fecha_registro;
	}

	public String getGuia_remision() {
		return guia_remision;
	}

	public void setGuia_remision(String guia_remision) {
		this.guia_remision = guia_remision;
	}
	
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public Transporte getTransporte() {
		return this.transporte;
	}

	public void setTransporte(Transporte transporte) {
		this.transporte = transporte;
	}

	public Tienda getLocal_partida() {
		return local_partida;
	}

	public void setLocal_partida(Tienda local_partida) {
		this.local_partida = local_partida;
	}

	public Tienda getLocal_entrega() {
		return local_entrega;
	}

	public void setLocal_entrega(Tienda local_entrega) {
		this.local_entrega = local_entrega;
	}
	
	
	
	public Integer getCantidad_activos() {
		return this.cantidad_activos;
	}

	public void setCantidad_activos(Integer cantidad_activos) {
		this.cantidad_activos = cantidad_activos;
	}
	
}
