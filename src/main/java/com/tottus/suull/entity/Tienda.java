package com.tottus.suull.entity;

import java.util.ArrayList;

import javax.persistence.Id;

import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tienda")

public class Tienda {

	@Transient
    public static final String SEQUENCE_NAME = "tienda_sequence";
	
	@Id
    private String id;
	
	private String nombre;
	private String ruc;
	private String direccion;
	private Integer departamento;
	private Integer provincia;
	private Integer distrito;
	private Integer numero;
	
	private Integer cantidad_activos;
	
	public ArrayList<Stock> stocks; 
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void updateActivos() {
		this.cantidad_activos=0;
		for(int i=0;i<this.stocks.size();i++) {
			this.cantidad_activos+=stocks.get(i).getStock();
		}
		System.out.print("this.cantidad_activos = "+this.cantidad_activos);
	}
	public Integer getStockBySKU(String sku){
		for(int i=0;i<stocks.size();i++) {
			if(stocks.get(i).getSku().equals(sku)) {
				return stocks.get(i).getStock();
			}
		}
		return 0;
	}
	public void setStockBySKU(String sku,Integer stock){
		for(int i=0;i<stocks.size();i++) {
			if(stocks.get(i).getSku().equals(sku)) {
				stocks.get(i).setStock(stock);
			}
		}
	}
	/*
	public Integer getTotalStock(){
		return 10;//total;
	}*/
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Integer getDepartamento() {
		return departamento;
	}
	public void setDepartamento(Integer departamento) {
		this.departamento = departamento;
	}
	public Integer getProvincia() {
		return provincia;
	}
	public void setProvincia(Integer provincia) {
		this.provincia = provincia;
	}
	public Integer getDistrito() {
		return distrito;
	}
	public void setDistrito(Integer distrito) {
		this.distrito = distrito;
	}
	
}
