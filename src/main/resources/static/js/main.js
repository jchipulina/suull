var dtolang = {
    	"search": "Buscar",
    	"paginate": {
            "first":      "Primero",
            "last":       "Último",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "lengthMenu": "Mostrando _MENU_",
        "zeroRecords": "No encontrado",
        "info": "Mostrando pagina _PAGE_ de _PAGES_",
        "infoEmpty": "No se encontraron registros",
        "infoFiltered": "(filtrado de _MAX_ registros)"
    }